#!/bin/bash

dbName="online_shop"
today=$(date +%Y-%m-%d)
echo 'Create .gz backup database $dbName' 
pg_dump -h localhost $dbName | gzip > "backup_$today.gz"
echo 'Upload .gz backup to google disk' 
gdrive upload "backup_$today.gz"
echo 'Remove .gz backup database $dbName' 
rm -f "backup_$today.gz"

