module.exports = {
  apps : [
    {
      name      : 'ekiosk',
      script    : 'ekiosk/app.js',
      watch: [
	      "ekiosk/assets/images/**/*"
      ]
    },
    {
      name      : 'ekiosk-admin',
      script    : 'ekiosk-admin/app.js',
    }

  ],
};
