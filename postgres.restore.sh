#!/bin/bash

today=$(date +%Y-%m-%d)
newDbName=${1:-testdb}
backupFileGz=${2:-"backup_$today.gz"}
psql -c "create database $newDbName;"
gunzip -c $backupFileGz | psql $newDbName
