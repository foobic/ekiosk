/*message*
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */

// Authentication module.
var auth = require('http-auth');
var basic = auth.basic({
  realm: "Simon Area."
}, function (username, password, callback) { // Custom authentication.
  callback(username === "KolyaDima" && password === "DimaKolya");
});

module.exports.policies = {
  '*':['cookieAuth',auth.connect(basic),'default','nocache']
};
