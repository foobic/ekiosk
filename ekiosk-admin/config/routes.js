/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

    /***************************************************************************
     *                                                                          *
     * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
     * etc. depending on your default view engine) your home page.              *
     *                                                                          *
     * (Alternatively, remove this and add an `index.html` file in your         *
     * `assets` directory)                                                      *
     *                                                                          *
     ***************************************************************************/

    '/': {
        controller: 'DashboardController',
        action: 'index'
        // locals:{
        //     layout: false
        // }
    },
    '/statistics': {
        controller: 'StatisticsController',
        action: 'index'
    },
    '/statistics/getReportByDateRange':{
          controller: 'StatisticsController',
          action: 'getReportByDateRange'
    },
    '/help': {
        // controller: 'StatisticsController',
        // action: 'index'
        view: 'help'
    },
    // USERS
    'GET /users/list': {
        controller: 'UsersController',
        action: 'showAll'
    },
  'GET /users/editOne/:id': {
        controller: 'UsersController',
        action: 'showEditOne'
    },
  'POST /users/editOne/:id': {
        controller: 'UsersController',
        action: 'editOne'
    },
    'GET /users/addOne': {
        controller: 'UsersController',
        action: 'showAddOne'
    },
    'POST /users/addOne': {
        controller: 'UsersController',
        action: 'addOne'
    },
    'DELETE /users/deleteOne/:id': {
        controller: 'UsersController',
        action: 'deleteOne'
    },
    // BLOG
    '/blog/list': {
        controller: 'BlogArticlesController',
        action: 'showAll'
    },
    'GET /blog/editOne/:id': {
        controller: 'BlogArticlesController',
        action: 'showEdit'
    },
    'POST /blog/editOne/:id': {
        controller: 'BlogArticlesController',
        action: 'editOne'
    },
    'POST /blog/addOne': {
        controller: 'BlogArticlesController',
        action: 'addOne'
    },
    // USERS
    '/categories/list': {
        controller: 'CategoriesController',
        action: 'showAll'
    },
    'GET /categories/addOne': {
        controller: 'CategoriesController',
        action: 'showAddOne'
    },
    'POST /categories/addOne': {
        controller: 'CategoriesController',
        action: 'addOne'
    },
    'GET /categories/editOne/:id': {
        controller: 'CategoriesController',
        action: 'showEditOne'
    },
    'POST /categories/editOne/:id': {
        controller: 'CategoriesController',
        action: 'editOne'
    },
    '/DELETE /categories/deleteOne/:id': {
        controller: 'CategoriesController',
        action: 'deleteOne'
    },
    // Goods
    'GET /goods/list': {
        controller: 'GoodsController',
        action: 'showAll'
    },
    'GET /goods/addOne': {
        controller: 'GoodsController',
        action: 'showAddOne'
    },
    'POST /goods/addOne': {
        controller: 'GoodsController',
        action: 'addOne'
    },
    'GET /goods/editOne/:id': {
        controller: 'GoodsController',
        action: 'showEditOne'
    },
    'POST /goods/editOne/:id': {
        controller: 'GoodsController',
        action: 'editOne'
    },
    // Orders
    'GET /orders/list': {
        controller: 'OrdersController',
        action: 'showAll'
    },

    'GET /orders/editOne/:id': {
        controller: 'OrdersController',
        action: 'showOne'
    },
    'POST /orders/editOne/:id': {
        controller: 'OrdersController',
        action: 'editOne'
    },
    'DELETE /orders/deleteOne/:id': {
        controller: 'OrdersController',
        action: 'deleteOne'
    },
    // 'POST /uploadImages':{
    //     controller: 'GoodsController',
    //     action: 'uploadImages'
    // }


    // providers
    'GET /providers/list': {
        controller: 'ProvidersController',
        action: 'showAll'
    },
    'GET /providers/addOne': {
        controller: 'ProvidersController',
        action: 'showAddOne'
    },
    'POST /providers/addOne': {
        controller: 'ProvidersController',
        action: 'addOne'
    },
    'GET /providers/editOne/:id': {
        controller: 'ProvidersController',
        action: 'showEditOne'
    },
    'POST /providers/editOne/:id': {
        controller: 'ProvidersController',
        action: 'editOne'
    },
    'DELETE /providers/deleteOne/:id': {
        controller: 'ProvidersController',
        action: 'deleteOne'
    },
    // delivers
    'GET /delivers/list': {
        controller: 'DeliversController',
        action: 'showAll'
    },
    'GET /delivers/addOne': {
        controller: 'DeliversController',
        action: 'showAddOne'
    },
    'POST /delivers/addOne': {
        controller: 'DeliversController',
        action: 'addOne'
    },
    'GET /delivers/editOne/:id': {
        controller: 'DeliversController',
        action: 'showEditOne'
    },
    'POST /delivers/editOne/:id': {
        controller: 'DeliversController',
        action: 'editOne'
    },
    'DELETE /delivers/deleteOne/:id': {
        controller: 'DeliversController',
        action: 'deleteOne'
    },
    // nomenclatureOfGood
    'GET /nomenclatures/list': {
        controller: 'NomenclaturesOfGoodsController',
        action: 'showAll'
    },
    'GET /nomenclatures/addOne': {
        controller: 'NomenclaturesOfGoodsController',
        action: 'showAddOne'
    },
    'POST /nomenclatures/addOne': {
        controller: 'NomenclaturesOfGoodsController',
        action: 'addOne'
    },
    'GET /nomenclatures/editOne/:id': {
        controller: 'NomenclaturesOfGoodsController',
        action: 'showEditOne'
    },
    'POST /nomenclatures/editOne/:id': {
        controller: 'NomenclaturesOfGoodsController',
        action: 'editOne'
    },
    'DELETE /nomenclatures/:id':{
        controller: 'NomenclaturesOfGoodsController',
        action: 'deleteOne'
    },
    'GET /nomenclatures/removeImages/:id':{
        controller: 'NomenclaturesOfGoodsController',
        action: 'removeImages'
    },

  'POST /refreshHomepageLists': {
        controller: 'DashboardController',
        action: 'refreshHomepageLists'
    },


};
