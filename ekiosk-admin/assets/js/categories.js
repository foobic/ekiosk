// function addFilterValue(place, id, value){
//     let newDiv = $('<div></div>');
//     let newId = $('<input />',{ type: 'number',
//         value: id,
//     }) ;
//     $(newId).attr('data-filter-values-id', 1);
//     let newValue = $('<input />',{
//         type: 'text',
//         value: value
//     }) ;
//     $(newValue).attr('data-filter-values-val', 1);
//     let deleteNewDiv = $('<a />',{
//         class: 'btn btn-danger btn-xs remove-this-value',
//         text: 'Del',
//     }) ;
//     $(newDiv).append([newId, newValue,deleteNewDiv]);
//     $(place).append(newDiv)
// }
// function increaseWindow(){
//     $("#page-wrapper").height($(document).height())
// }
// function decreaseWindow(){
//     $("#page-wrapper").height($(document).height()-500)
// }
// function insertFilter(name, title, show, type, exitsValues){
//     let filterBlock = $('.exits-filters');
//     let filterItem = $('<div />',{ class: 'filter-item'});
//
//     //form group 0
//     let formGroup0 = $('<div />',{ class: 'form-group'});
//
//     let fg0Label = $('<label />',{ for: 'name', text: 'Name', }) ;
//     let fg0Input = $('<input />',{
//         type: 'text',
//         value: name,
//         class: 'form-control filter-name',
//         name: 'name',
//         minlength: '6',
//         maxlength: '255'
//     }) ;
//     $(fg0Input).attr('data-filter', 1);
//
//     console.log(exitsValues)
//
//     $(formGroup0).append([fg0Label,fg0Input]);
//     //form group 1
//     let formGroup1 = $('<div />',{ class: 'form-group'});
//
//     let fg1Label = $('<label />',{ for: 'title', text: 'Title', }) ;
//     let fg1Input = $('<input />',{
//         type: 'text',
//         value: title,
//         class: 'form-control filter-title',
//         name: 'title',
//         minlength: '6',
//         maxlength: '255',
//     }) ;
//     $(fg1Input).attr('data-filter', 1);
//     $(formGroup1).append([fg1Label,fg1Input]);
//
//     //FORM GROUP 2
//     let formGroup2 = $('<div />',{ class: 'form-group'});
//
//     let fg2Label = $('<label />',{ for: 'show', text: 'Show'}) ;
//     let fg2Select = $('<select />',{
//         class: 'form-control input-large filter-show',
//         name: 'show',
//         id: 'show'
//     }) ;
//     let fg2Option0 = $('<option />',{ value: "0", text: "false" });
//     let fg2Option1 = $('<option />',{ value: "1", text: "true" });
//     if (show === false){
//         $(fg2Option0).attr('selected', true);
//     }else if(show === true ){
//         $(fg2Option1).attr('selected', true);
//     }
//     $(fg2Select).attr('data-filter', 1);
//     fg2Select.append([fg2Option0,fg2Option1]);
//     $(formGroup2).append([fg2Label,fg2Select]);
//
//     // FORM GROUP 3
//     let formGroup3 = $('<div />',{ class: 'form-group'});
//
//     let fg3Label = $('<label />',{ for: 'type', text: 'Type'}) ;
//     let fg3Select = $('<select />',{
//         class: 'form-control input-large',
//         name: 'type',
//         id: 'type'
//     }) ;
//     let fg3Option1 = $('<option />',{ value: "1", text: "Radio" });
//     let fg3Option2 = $('<option />',{ value: "2", text: "Checkbutton" });
//     if (type === 1){
//         $(fg3Option1).attr('selected', true);
//     }else if(type === 2 ){
//         $(fg3Option2).attr('selected', true);
//     }
//     $(fg3Select).attr('data-filter', 1);
//     fg3Select.append([fg3Option1,fg3Option2]);
//     $(formGroup3).append([fg3Label,fg3Select]);
//
//     // FORM GROUP 4
//     let formGroup4 = $('<div />',{ class: 'form-group', });
//     let fg4Label = $('<label />',{ text: 'Values' });
//     let fg4Input1 = $('<input />',{
//         type: 'number',
//         value: '1',
//         step:'1',
//         class: 'new-value-id',
//         name: 'quantity',
//         min: '1',
//     });
//     let fg4Input2 = $('<input />',{
//         type: 'text',
//         class: 'new-value-name',
//         value: 'New Value',
//     }) ;
//     let fg4link = $('<a />',{
//         class: "btn btn-primary btn-xs add-new-value",
//         text: 'Add'
//     }) ;
//     let fg4ExitstFilterValues = $('<div />',{
//         class: "exitsts-filter-values"
//     }) ;
//
//     $(formGroup4).append([fg4Label,$('<br>'),fg4Input1, fg4Input2, fg4link, fg4ExitstFilterValues]);
//     for( let i = 0; i < exitsValues.length; i++ ){
//         addFilterValue(fg4ExitstFilterValues,exitsValues[i].position,exitsValues[i].value )
//     }
//     // FormGroup5 - DELETE
//     let formGroup5 = $('<div />',{ class: 'form-group', });
//     let fg5delete = $('<button />',{
//         class: 'btn btn-danger deleteFilterButton',
//     });
//     $(fg5delete).text('DELETE')
//     $(formGroup5).append($(fg5delete));
//
//     let nameHeader = $('<h2 />').text(name);
//     $(filterItem).append([$(nameHeader),$(formGroup0),$(formGroup1), $(formGroup2), $(formGroup3),
//         $(formGroup4), $(formGroup5)]);
//     $(filterBlock).append([$(filterItem)]);
//
//     increaseWindow()
// }

$(document).on('click', '.deleteCategory', function() {
  const deleteID = $(this).attr('data-id');
  const deleteCategory = this;
  $.ajax({
    type: 'DELETE',
    url: '/categories/deleteOne/' + deleteID,
    success(data) {
      const deletedRow = $(deleteCategory).closest('tr')[0];
      $(deletedRow).fadeOut(600, function() {
        $(this).remove();
      });
    },
    error(xhr, a, b) {
      window.alert(xhr.responseJSON.error);
    },
  });
});


// function createExitstsFilters(){
//     var categoryRecord = cats.filter(function(obj) {
//         return obj.id == category.id;
//     })[0];
//     let filters = JSON.parse(categoryRecord.filters);
//     let filtersKeys = Object.keys(filters)
//     let filtersLen = filtersKeys.length;
//     let curFilter;
//     for (let i = 0; i < filtersLen; i++){
//         curFilter = filters[filtersKeys[i].toString()];
//         insertFilter(
//             filtersKeys[i].toString(),
//             curFilter.title,
//             curFilter.show,
//             curFilter.type,
//             curFilter.values
//         );
//     }
// }
//
// $(document).on('click',".addFilterButton", function(){
//     let filterDataRaw = $('.addFilter');
//     let filterValuesIdRaw = $('.exitsts-filter-values').find('*[data-filter-values-id=1]');
//     let filterValuesRaw = $('.exitsts-filter-values').find('*[data-filter-values-val=1]');
//     console.log(filterValuesIdRaw)
//     let filterData = [];
//     for(let i=0;i<filterDataRaw.length;i++){
//         filterData.push($(filterDataRaw[i]).val())
//     }
//     let tmpArr = []
//     let tmpObj = {};
//     for(let i=0;i<filterValuesIdRaw.length;i++){
//         tmpObj['position']= parseInt($(filterValuesIdRaw[i]).val())
//         tmpObj['value']= $(filterValuesRaw[i]).val()
//         tmpArr.push(tmpObj);
//     }
//     filterData.push(tmpArr);
//     console.log(filterData);
//     insertFilter(
//         filterData[0],
//         filterData[1],
//         filterData[2],
//         filterData[3],
//         filterData[4],
//     );
// } );
//
// $(document).on('click',".deleteFilterButton", function(){
//     $(this).closest('.filter-item').remove();
//     decreaseWindow();
// } );
//
// $(document).on('click',".remove-this-value", function(){
//     $(this).closest('div').remove();
// } );
//
// $(document).on('click',".add-new-value", function(){
//     let id = $(this).siblings("input[type=number]").val();
//     let value = $(this).siblings("input[type=text]").val();
//     let blockToInsert = $(this).siblings(".exitsts-filter-values");
//     addFilterValue(blockToInsert, id, value);
// } );
//

$(document).on('click', '.saveChanges', () => {
  // let filterJSON = {};
  const catName = $('[name=categoryName]').val();
  const catParentID = $('[name=categoryParentId]').val();
    console.log(catParentID);
    console.log(category.id);
  if (catParentID == category.id) {
    window.alert('Category cannot have self as a parent');
  } else {
  // let filtersScratch = $('.exits-filters .filter-item');
  // let filtersLength = filtersScratch.length;
  // let filterData,filterValuesId,filterValues, tmpObj;
  // for (let i = 0; i < filtersLength; i++){
  //     filterData = $(filtersScratch[i]).find('*[data-filter=1]');
  //     filterValuesId = $(filtersScratch[i]).find('*[data-filter-values-id=1]');
  //     filterValues = $(filtersScratch[i]).find('*[data-filter-values-val=1]');
  //     filterName = $(filterData[0]).val()
  //
  //     filterJSON[filterName] = {};
  //     filterJSON[filterName]["title"] = $(filterData[1]).val();
  //     filterJSON[filterName]["show"] = Boolean(parseInt($(filterData[2]).val()));
  //     filterJSON[filterName]["type"] = parseInt($(filterData[3]).val());
  //
  //     tmpObj = [];
  //     for (let j=0;j<filterValuesId.length;j++){
  //         tmpObj.push({"position":parseInt($(filterValuesId[j]).val()), "value":$(filterValues[j]).val()})
  //     }
  //     filterJSON[filterName]["values"]=tmpObj;
  // }
    jQuery.ajax({
      url: '/categories/editOne/' + category.id,
      type: 'POST',
      data: {
        'name': catName,
        'parentId': catParentID,
      //     "filters": JSON.stringify(filterJSON)
      },
      success() {
        alert('Category successfully changed');
      },
      error(xhr) {
        window.alert('ERROR: ', xhr.resposeText);
      },
    });
  }
});

$(document).ready(() => {
  // createExitstsFilters();
});
