$(document).ready(() => {
  $('#nomenclatureIdSelect option[value=' + nomenclatureId + ']').attr('selected', true);
  $('#providerIdSelect option[value=' + good.providerId + ']').attr('selected', true);
  $('#deliverIdSelect option[value=' + deliver.id + ']').attr('selected', true);
});

function nomenclaturesList() {
  const myList = $('.nomenclatures-list')[0];
  // console.log(nomenclatures);
  const optionValues = [];


  // console.log(myList);
  for (let i = 0; i < nomenclatures.length; i++) {
    $(myList).append($('<option></option>')
      .attr('value', nomenclatures[i].id)
      .text(nomenclatures[i].categoryId + ': ' + nomenclatures[i].id + ': ' + nomenclatures[i].name));
  }
}
function providersList() {
  const myList = $('.providers-list')[0];
  // console.log(nomenclatures);
  const optionValues = [];
  for (let i = 0; i < providers.length; i++) {
    $(myList).append($('<option></option>')
      .attr('value', providers[i].id)
      .text(providers[i].id + ': ' + providers[i].name));
  }
}
function deliversList() {
  const myList = $('.delivers-list')[0];
  // console.log(nomenclatures);
  const optionValues = [];
  for (let i = 0; i < delivers.length; i++) {
    $(myList).append($('<option></option>')
      .attr('value', delivers[i].id)
      .text(delivers[i].id + ': ' + delivers[i].name));
  }
}
$(document).on('change', '#nomenclatureIdSelect', function() {
  $('input[name=nomenclatureId]').val($('option:selected', this).val());
});
$(document).on('change', '#providerIdSelect', function() {
  $('input[name=providerId]').val($('option:selected', this).val());
});
$(document).on('change', '#deliverIdSelect', function() {
  $('input[name=deliverId]').val($('option:selected', this).val());
});

$(document).on('click', '.deleteGood', function() {
  if (confirm('Are you sure you want to delete this Goods? ')) {
    const deleteID = $(this).attr('data-id');
    const deleteNumID = $(this).attr('data-nomenclatureId');
    const deleteGood = this;
    $.ajax({
      type: 'DELETE',
      url: '/goods/deleteOne/' + deleteID,
      success(data) {
        const deletedRow = $(deleteGood).closest('tr')[0];
        $(deletedRow).fadeOut(600, function() {
          $(this).remove();
        });
      },
      error(xhr) {
        window.alert(xhr.responseJSON.error);
      },
    });
  }
});
