$(document).on('click', '.deleteDeliver', function() {
  const deleteID = $(this).attr('data-id');
  const deleteDeliver = this;
  $.ajax({
    type: 'DELETE',
    url: '/delivers/deleteOne/' + deleteID,
    success(data) {
      const deletedRow = $(deleteDeliver).closest('tr')[0];
      $(deletedRow).fadeOut(600, function() {
        $(this).remove();
      });
    },
    error(xhr) {
      window.alert(xhr.responseJSON.error);
    },
  });
});


$(document).ready(() => {
  // createExitstsFilters();
});
