var lots_of_stuff_already_done;
$(document).ready(() => {
  $('.editOrAddForm').submit(function(e){
      event.preventDefault();
      const userphone = $('input[name=phone]').val();
      const username = $('input[name=name]').val();
      if (username.name < 2 || userphone.length != 19) {
        window.alert('Неправильно введены имя и/или телефон');
        return false;
      }
      event.currentTarget.submit();
  });
  $('.deleteUser').click(function() {
    const deleteID = $(this).attr('data-id');
    const deleteUser = this;
    $.ajax({
      type: 'DELETE',
      url: '/users/deleteOne/' + deleteID,
      success(data) {
        const deletedRow = $(deleteUser).closest('tr')[0];
        $(deletedRow).fadeOut(600, function() {
          $(this).remove();
        });
      },
      error(xhr) {
        window.alert(xhr.responseJSON.error);
      },
    });
  });

  $('.editUser').click(function() {
    // window.alert()
    // window.alert('fdajks')
    window.location.replace('/users/editOne/'+$(this).attr('data-id'))

  })
  // $('.editUser').click(function() {
  //   const curObj = $(this);
  //   const curRow = $(this).closest('tr')[0];
  //
  //   const state = $(curObj).data('pressed');
  //
  //   const inputs = $(curRow).find('input[data-show=1]');
  //   const spans = $(curRow).find('span[data-show=1]');
  //   const icons = $(curObj).children();
  //   // for input, if state == 0, i.e edit button clicked
  //   if (state === 0) {
  //     $(inputs[0]).val($(spans[0]).text());
  //     $(inputs[1]).val($(spans[1]).text());
  //
  //     // show and hide elements
  //     [
  //       $(spans[0]),
  //       $(spans[1]),
  //       $(icons[0]),
  //     ].forEach((item) => {
  //       item.hide();
  //     });
  //     [
  //       $(inputs[0]),
  //       $(inputs[1]),
  //       $(icons[1]),
  //     ].forEach((item) => {
  //       item.show();
  //     });
  //
  //     // for save user input, if state == 1, i.e save button clicked
  //   } else if (state === 1) {
  //     const inputName = $(inputs[0]).val();
  //     const inputEmail = $(inputs[1]).val();
  //     const userID = $(this).attr('data-id');
  //     $.ajax({
  //       type: 'PUT',
  //       url: '/users/' + userID + '?name=' + inputName + '&email=' + inputEmail,
  //       success(data) {
  //         console.log(data);
  //         $(spans[0]).text(inputName);
  //         $(spans[1]).text(inputEmail);
  //
  //         // show and hide elements
  //         [
  //           $(inputs[0]),
  //           $(inputs[1]),
  //           $(icons[1]),
  //         ].forEach((item) => {
  //           item.hide();
  //         });
  //         [
  //           $(spans[0]),
  //           $(spans[1]),
  //           $(icons[0]),
  //         ].forEach((item) => {
  //           item.show();
  //         });
  //       },
  //       // error(xhr, ajaxOptions, thrownError) {
  //       error(xhr) {
  //         window.alert('ERROR: ', xhr.resposeText);
  //       },
  //     });
  //   }
  //
  //   // changing state
  //   $(curObj).data('pressed', Number(!state));
  // });
});
