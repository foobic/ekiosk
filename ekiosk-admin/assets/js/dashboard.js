$(document).ready(() => {
  const salesList = [];
  const popularList = [];
  const newList = [];
  const allGoodsOptions = [];
  const configListIds = {
    salesListIds: [],
    popularListIds: [],
    newListIds: [],
  };
  configList.salesList.map((el) => {
    configListIds.salesListIds.push(el.id);
  });
  configList.popularList.map((el) => {
    configListIds.popularListIds.push(el.id);
  });
  configList.newList.map((el) => {
    configListIds.newListIds.push(el.id);
  });
  allGoods.map((el) => {
    salesList.push(new Option(el.name, el.id));
  });
  allGoods.map((el) => {
    popularList.push(new Option(el.name, el.id));
  });
  allGoods.map((el) => {
    newList.push(new Option(el.name, el.id));
  });
  $($('.salesList')[0]).append(salesList).val(configListIds.salesListIds);
  $($('.popularList')[0]).append(popularList).val(configListIds.popularListIds);
  $($('.newList')[0]).append(newList).val(configListIds.newListIds);
  $('.chosen-select').chosen({
    no_results_text: 'Oops, nothing found!',
    width: '95%',
    max_selected_options: 3,
  });
  $('.saveConfigList').click(() => {
    const salesList = ($($('.salesList')[0]).val()).map(el => parseInt(el));
    const popularList = ($($('.popularList')[0]).val()).map(el => parseInt(el));
    const newList = ($($('.newList')[0]).val()).map(el => parseInt(el));
    console.log(salesList);
    $.ajax({
      type: 'POST',
      url: '/refreshHomepageLists',
      data: {
        configList: {
          salesList,
          newList,
          popularList,
        },
      },
      success(data) {
        window.alert('ok');
      },
      error(xhr) {
        window.alert(xhr.responseJSON.error);
      },
    });
  });
});
