$(document).on('click', '.deleteProvider', function() {
  const deleteID = $(this).attr('data-id');
  const deleteProvider = this;
  $.ajax({
    type: 'DELETE',
    url: '/providers/deleteOne/' + deleteID,
    success(data) {
      const deletedRow = $(deleteProvider).closest('tr')[0];
      $(deletedRow).fadeOut(600, function() {
        $(this).remove();
      });
    },
    error(xhr, a, b) {
      window.alert(xhr.responseJSON.error);
    },
  });
});


$(document).ready(() => {
  // createExitstsFilters();
});
