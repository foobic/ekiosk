const myhost = 'http://104.211.230.230';
function listToTree(list) {
  let map = {},
      node,
      roots = [],
      i;
  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i; // initialize the map
    list[i].children = []; // initialize the children
  }
  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.parentId !== 0) {
      if (typeof list[map[node.parentId]] === 'undefined') {
        window.alert('Uncorrect parentId: ' + node.id);
        break;
      }
      list[map[node.parentId]].children.push(node);
    } else {
      roots.push(node);
    }
    // console.log(node);
  }
  return roots;
}

function addRoot(tree, root) {
  const newList = [root];
  newList[0].children = tree;
  return newList;
}

// $(document).on('load',() => {
// $("#page-wrapper").height($(document).height());
// });
$(document).ready(() => {
  $('#page-wrapper').height($(document).height());
  function setCookie(name, value, options) {
    options = options || {};

    let expires = options.expires;

    if (typeof expires === 'number' && expires) {
      const d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    valueIComponent(value);

    let updatedCookie = name + '=' + value;

    for (const propName in options) {
      updatedCookie += '; ' + propName;
      const propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += '=' + propValue;
      }
    }

    document.cookie = updatedCookie;
  }
});

