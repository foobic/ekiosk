$(document).ready(() => {
  $('.deleteArticle').click(function() {
    const deleteID = $(this).attr('data-id');
    const deleteArticle = this;
    $.ajax({
      type: 'DELETE',
      url: '/blogArticles/' + deleteID,
      success(data) {
        const deletedRow = $(deleteArticle).closest('tr')[0];
        console.log(data);
        $(deletedRow).fadeOut(600, function() {
          $(this).remove();
        });
      },
      // error(xhr, ajaxOptions, thrownError) {
      error(xhr) {
        window.alert('ERROR: ', xhr.resposeText);
      },
    });
  });
});
