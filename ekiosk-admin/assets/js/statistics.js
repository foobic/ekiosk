$(document).ready(() => {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
  });
  $('.input-daterange input').each(function() {
    $(this).datepicker({
      format: 'yyyy/mm/dd',
    });
  });
  $('.getReportBtn').click((e) => {
    e.preventDefault();
    const dateFromRaw = $('.dateFrom').val();
    const dateToRaw = $('.dateTo').val();
    const dateFrom = new Date(dateFromRaw);
    const dateTo = new Date(dateToRaw);
    const isValidDate = d => d instanceof Date && !isNaN(d);
    // const getnormdate = date => date.toISOString().slice(0, 10).split('-').join('/');
    // console.log(dateFrom);
    // console.log(typeof dateFrom);
    console.log();
    if (dateFrom > dateTo) {
      window.alert('Warning: Date from should be < than dateTo');
      return;
    } else if (!isValidDate(dateFrom) || !isValidDate(dateTo)) {
      window.alert('Please, fill all fields.');
      return;
    } else {
      jQuery.ajax({
        url: '/statistics/getReportByDateRange',
        type: 'POST',
        data: {
          search: {
            sortBy: $($('select[name=sortBy]')[0]).val(),
            sortDirection: $($('select[name=sortDirection]')[0]).val(),
          },
          daterange: {
            fromRaw: dateFromRaw,
            from: dateFrom,
            toRaw: dateToRaw,
            to: dateTo,
          },
        },
        success(result) {
          $('.reportByRangeResult').slideUp(100);
          $($('.linkToOrderListByRange')[0]).attr('href', `http://localhost:8080/orders/list?closed&startDate=${dateFromRaw}&endDate=${dateToRaw}`);
          $('.closedOrdersQuantity:first-child').html(result.closedOrdersQty);
          $('.avgOrderSum:first-child').html(result.avgSum);
          $('.totalGoodsQuantity:first-child').html(result.total.qty);
          $('.totalSum:first-child').html(result.total.sum);
          $('.totalProfit:first-child').html(result.total.profit);
          const tbody = $($('.listOfGoods')[0]);
          tbody.html('');
          const height = 1000 + 50 * result.goods.length;
          $('#page-wrapper').attr({
            'style': 'height:' + height + 'px !important;',
          });
          // tbody.append($('<tr></tr>').append('<td></td>'));
          for (let i = 0; i < result.goods.length; i++) {
            tbody.append($('<tr></tr>')
              .append($('<td></td>').append($('<a/>').attr('href', '/goods/editOne/' + result.goods[i].goodId).html(result.goods[i].goodId)))
              .append($('<td></td>').append($('<a/>').attr('href', '/nomenclatures/editOne/' + result.goods[i].nomenclatureId).html(result.goods[i].nomenclatureName)))
              .append($('<td></td>').html(result.goods[i].realPrice))
              .append($('<td></td>').html(result.goods[i].price))
              .append($('<td></td>').html(result.goods[i].qty))
              .append($('<td></td>').html(result.goods[i].sum))
              .append($('<td></td>').html(result.goods[i].profit))
            );
            // $('.listOfGoods:first-child').append($('<tr></tr>').html('11'));
          }
          console.log(result);
          $('.reportByRangeResult').slideDown(300);
        },
        error(xhr) {
          window.alert('ERROR: ', xhr.resposeText);
        },
      });
    }
  });
});
