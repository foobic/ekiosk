$(document).ready(() => {
  // const images = [];
$($('form')[1]).attr('action', myhost + ":80/uploadImages/"+nomenclature.id)
  $('#catIdSelect option[value=' + category + ']').attr('selected', true);
    console.log('afsn;dflkajskfld;ajfjsdfl;ajs;fjaskldfjas;dfl');

  console.log(';asjfdaksldfj;asfdfdsajfa;dslk',images);
  for (let i = 0; i < images.length; i++) {
    const el = $('<a />').attr('href', myhost + images[i]).text(images[i]);
    $('.existsImages').append(el);
  }
});

$(document).on('change', 'input[name=price],input[name=discount]', () => {
  const price = parseFloat($('input[name=price]').val());
  const realPriceInput = $('input[name=realPrice]');
  const discount = parseFloat($('input[name=discount]').val());
  const realPrice = (price * (1 - (discount / 100))).toFixed(2);
  if (discount < 0 || price <= 0 || realPrice <= 0) {
    window.alert('discount or price are not defined OR real price < 0');
  } else {
    $(realPriceInput).val(realPrice);
  }
});

$(document).on('change', '#catIdSelect', function() {
  $('input[name=catId]').val($('option:selected', this).val());
});

$(document).on('click', '.deleteNomenclature', function() {
  const deleteID = $(this).attr('data-id');
  const deleteGood = this;
  $.ajax({
    type: 'DELETE',
    url: '/nomenclatures/' + deleteID,
    success(data) {
      const deletedRow = $(deleteGood).closest('tr')[0];
      $(deletedRow).fadeOut(600, function() {
        $(this).remove();
      });
    },
    error(xhr,a,b) {
        window.alert(xhr.responseJSON.error);
    },
  });
});
