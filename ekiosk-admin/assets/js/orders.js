$(document).ready(() => {
    $('.statusCodeOption').find('option[value=' + orderStatusCode + ']')
      .attr('selected', true);
});

$(document).on('click', '.deleteOrder', function() {
  if (confirm('Are you sure you want to delete this ORDER ??? ')) {
    const deleteID = $(this).attr('data-id');
    const deleteOrder = this;
    $.ajax({
      type: 'DELETE',
      url: '/orders/deleteOne/' + deleteID,
      success(data) {
        const deletedRow = $(deleteOrder).closest('tr')[0];
        console.log(data);
        $(deletedRow).fadeOut(600, function() {
          $(this).remove();
        });
      },
      error(xhr) {
        window.alert('ERROR: ', xhr.resposeText);
      },
    });
  }
});
