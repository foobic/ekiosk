$(document).ready(() => {
  const catsTree = addRoot(listToTree(cats), catsRoot);

  let level = 0;
  const optionValues = [];
  function my_recoursion(arr) {
    Object.keys(arr).forEach((key, index) => {
      optionValues.push([
        arr[key].id,
        ' >>> '.repeat(level) + arr[key].name,
      ]);
      if (arr[key]['children'] !== []) {
        level += 1;
        my_recoursion(arr[key]['children'], level);
        level -= 1;
      }
    });
  }
  my_recoursion(catsTree);

  const myList = $('.categories-list')[0];

  console.log(myList);
  for (let i = 0; i < optionValues.length; i++) {
    if (optionValues[i][0] === category.parentId) {
      $(myList).append($('<option selected></option>')
        .attr({ 'value': optionValues[i][0], 'class': 'text-primary' })
        .text(optionValues[i][0]+': '+optionValues[i][1]));
    } else {
      $(myList).append($('<option></option>')
        .attr('value', optionValues[i][0])
        .text(optionValues[i][0]+': '+optionValues[i][1]));
    }
  }
});
