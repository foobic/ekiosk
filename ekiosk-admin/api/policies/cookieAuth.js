/* Goods*
 *  * Sets no-cache header in response.
 *   */
module.exports = async function(req, res, next) {
  if (req.cookies.billion === '1') {
    next();
  } else {
    return res.send(404);
  }
};
