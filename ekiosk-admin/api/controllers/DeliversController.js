/**
 * DeliversController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  // showAll
  async showAll(req, res) {
    await Delivers.find({ }, (err, delivers) => {
      if (err) {
        return res.serverError(err);
      } else {
        // return results to view
        return res.view('delivers/list', {
          data: delivers,
        });
      }
    });
  },
  // showAddOne
  async showAddOne(req, res) {
    return res.view('delivers/addOne');
  },

  // addOne
  async addOne(req, res) {
    // just create request with params which recieved from view
    await Delivers.create({
      name: req.param('name'),
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'Deliver successfully created',
      });
    });
  },
  // showEditOne
  async showEditOne(req, res) {
    await Delivers.findOne({
      where: { id: req.param('id') },
    }, (err, deliver) => {
      if (err) {
        return res.serverError(err);
      }
      if (deliver === undefined) {
        return res.view('alerts/error', {
          alertMsg: 'Wrong deliver edit url.',
        });
      }
      return res.view('delivers/editOne', {
        data: deliver,
      });
    });
  },
  // editOne
  async editOne(req, res) {
    // just update request to db wia WaterLine
    await Delivers.update({
      id: req.param('id'),
    }).set({
      name: req.param('name'),
    });
    return res.redirect('/delivers/editOne/' + req.param('id'));
  },

  async deleteOne(req, res) {
    Delivers.destroy({
      id: parseInt(req.param('id')),
    }, (err, destroyedDeliver) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Deliver successfully destroyed');
      }
    });
  },


};

