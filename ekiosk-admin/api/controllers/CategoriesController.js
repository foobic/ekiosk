/**
 * CategoriesController
 *
 * @description :: Server-side logic for managing Categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const CommonServices = require('../services/CommonServices');
const CategoriesServices = require('../services/CategoriesServices');

module.exports = {

  // show /categories/showAll  page
  async showAll(req, res) {
    // just get all categories from db
    await Categories.find({
      sort: 'parentId asc',
    }, (err, categories) => {
      if (err) {
        return res.serverError(err);
      } else {
        // console.log(categories);
        if (categories.length !== 0) {
          for (let i = 0; i < categories.length; i++) {
            // converting bytes to utf8
            categories[i].filters = CommonServices
              .stringFromUTF8Array(categories[i].filters)
              .substring(0, 50) + '...';
          }
        }
        // return results to view
        return res.view('categories/list', {
          data: categories,
        });
      }
    });
  },

  // show /categories/addOne  page
  async showAddOne(req, res) {
    // find all categories for doing tree of categories
    await Categories.find({ }, (err, categories) => res.view('categories/addOne', {
      cats: categories,
      catsRoot: { id: 0, name: 'root', parentId: 0 },
      category: { id: 0, name: 'root', parentId: 0 }, // edit category
    }));
  },

  // addOne category functionality
  async addOne(req, res) {
    const bfilters = new Buffer(JSON.stringify({ }), 'utf-8');
    // just create request with params which recieved from view
    await Categories.create({
      name: req.param('name'),
      parentId: parseInt(req.param('parentId')),
      filters: bfilters,
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'Category successfully created',
      });
    });
  },

  // show /categories/editOne page
  async showEditOne(req, res) {
    // TODO: REFATORING: it should contain one request for all categroies and
    // just filter this record for find one
    // all categories, needs to show tree of categories
    const cats = await Categories.find({
      select: ['id', 'name', 'parentId', 'filters'],
    });
    for (let i = 0; i < cats.length; i++) {
      cats[i].filters = CommonServices
        .stringFromUTF8Array(cats[i].filters);
    }
    // getting a category
    await Categories.findOne({
      where: { id: req.param('id') },
    }, (err, category) => {
      if (err) {
        return res.serverError(err);
      }
      // console.log(category);
      // console.log(cats);
      if (category === undefined) {
        return res.view('alerts/error', {
          alertMsg: 'Wrong category edit url.',
        });
      }
      return res.view('categories/editOne', {
        category, // edit category
        catsRoot: { id: 0, name: 'root', parentId: 0 },
        cats, // all categories
      });
    });
  },
  // /categories/editOne functionality
  async editOne(req, res) {
    // just update request to db wia WaterLine
    await Categories.update({
      id: req.param('id'),
    }).set({
      name: req.param('name'),
      parentId: req.param('parentId'),
    });
    return res.redirect('/categories/editOne/' + req.param('id'));
  },
  async deleteOne(req, res) {
    // console.log( parseInt(req.param('id')) );
    Categories.destroy({
      id: parseInt(req.param('id')),
    }, (err, destroyedCategory) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Categoy successfully destroyed');
      }
    });
  },

};
