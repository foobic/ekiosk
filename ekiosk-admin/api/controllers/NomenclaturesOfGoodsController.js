/**
 * NomenclaturesOfGoodsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  // showAll
  async showAll(req, res) {
    // getting all goods
    await NomenclaturesOfGoods.find({
      // sort: 'parentId asc',
    }, (err, nomenclaturesOfGoods) =>
    // return goods to view as obj
      res.view('nomenclaturesOfGoods/list', {
        data: nomenclaturesOfGoods,
      })
    );
  },
  // showAddOne
  async showAddOne(req, res) {
    // find all categories and sort it by desc
    await Categories.find({
      sort: 'parentId asc',
    }, (err, categories) => {
      for (let i = 0; i < categories.length; i++) {
        categories[i].filters = CommonServices
          .stringFromUTF8Array(categories[i].filters)
          .substring(0, 50) + '...';
      }
      return res.view('nomenclaturesOfGoods/addOne', {
        cats: categories,
        catsRoot: {
          id: 0,
          name: 'root',
          parentId: 0,
        },
        category: {
          id: 0,
          name: 'root',
          parentId: 0,
        }, // edit category
      });
    });
  },
  // addOne
  async addOne(req, res) {
    // create request with params which was recieved from view by by POST request
    await NomenclaturesOfGoods.create({
      name: req.param('name'),
      desc: req.param('desc'),
      categoryId: parseInt(req.param('catId')),
      manufactor: req.param('manufactor'),
      images: JSON.stringify([]),
      // properties: JSON.stringify({}),
      // customProperties: JSON.stringify({}),
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'nomenclature successfully created',
      });
    });
  },
  // showEditOne
  async showEditOne(req, res) {
    const cats = await Categories.find({
      select: ['id', 'name', 'parentId', 'filters'],
    });

    await NomenclaturesOfGoods.findOne({
      where: { id: req.param('id') },
    }, (err, nomenclature) => {
      // if find nothing return alert
      if (typeof nomenclature === 'undefined' || nomenclature === 0) {
        return res.view('alerts/error', {
          alertMsg: 'Nomenclature doesn\'t exists',
        });
      }
      // convert bytes to str
      nomenclature.images = CommonServices.stringFromUTF8Array(nomenclature.images);
      return res.view('nomenclaturesOfGoods/editOne', {
        nomenclature,
        cats,
        catsRoot: { id: 0, name: 'root', parentId: 0 },
        category: nomenclature.categoryId,
      });
    });
  },
  // editOne
  async editOne(req, res) {
    const id = req.param('id');
    await NomenclaturesOfGoods.update({ id }).set({
      name: req.param('name'),
      desc: req.param('desc'),
      categoryId: parseInt(req.param('catId')),
      manufactor: req.param('manufactor'),
      images: JSON.stringify([]),
    });
    // then -> redirect back
    return res.redirect('/nomenclatures/editOne/' + id);
  },
  async removeImages(req, res) {
    const id = req.param('id');
    await NomenclaturesOfGoods.update({ id }).set({
      images: JSON.stringify([]),
    });
    // then -> redirect back
    return res.redirect('/nomenclatures/editOne/' + id);
  },
  // deleteOne
  async deleteOne(req, res) {
    console.log(parseInt(req.param('id')));
    NomenclaturesOfGoods.destroy({
      id: parseInt(req.param('id')),
    }, (err) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Nomenclature successfully destroyed');
      }
    });
  },
};

