/**
 * UsersController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


const NotImplementedError = require('../services/notImplementedError.js');

module.exports = {

  // find and show all users. /users page
  async showAll(req, res) {
    await Users.find({ sort: 'id asc' }, (err, users) => {
      if (err) {
        return res.serverError(err);
      }
      // console.log(users);
      return res.view('users/list', { data: users });
    });
  },
  // showAddOne
  async showAddOne(req, res) {
    return res.view('users/addOne');
  },

  // addOne
  async addOne(req, res) {
    // just create request with params which recieved from view
    await Users.create({
      name: req.param('name'),
      phone: req.param('phone'),
      email: req.param('email'),
      userDesc: req.param('userDesc'),
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'User successfully created',
      });
    });
  },
  // showEditOne
  async showEditOne(req, res) {
    await Users.findOne({
      where: { id: req.param('id') },
    }, async(err, user) => {
      if (err) {
        return res.serverError(err);
      }
      if (user === undefined) {
        return res.view('alerts/error', {
          alertMsg: 'Wrong user edit url.',
        });
      }
      const orders = await Orders.find({ 'customer': user.id });
      console.log(orders.length);
      return res.view('users/editOne', {
        data: user,
        orders,
      });
    });
  },
  // editOne
  async editOne(req, res) {
    // just update request to db wia WaterLine
    await Users.update({
      id: req.param('id'),
    }).set({
      name: req.param('name'),
      phone: req.param('phone'),
      email: req.param('email'),
      userDesc: req.param('userDesc'),
    });
    return res.redirect('/users/editOne/' + req.param('id'));
  },

  async deleteOne(req, res) {
    Users.destroy({
      id: parseInt(req.param('id')),
    }, (err, destroyedProvider) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('User successfully destroyed');
      }
    });
  },
};
