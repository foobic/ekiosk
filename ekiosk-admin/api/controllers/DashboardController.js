/*
 * DashboardController
 *
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  // show some data
  async index(req, res) {
    const usersQuantity = await Users.count({});
    const articlesQuantity = await BlogArticles.count({});
    const goodsAllQuantity = await Goods.count({});
    const goodsQuantity = await Goods.count({
      where: { 'quantity': { '>': 0 } },
    });
    const openedOrders = await Orders.find({ where: { 'statusCode': 0 } });
    const processedOrders = await Orders.find({ where: { 'statusCode': 1 } });
    const closedOrders = await Orders.find({ where: { 'statusCode': 2 } });
    const allGoods = await Goods.find({
      select: ['nomenclatureId', 'id', 'salesFlag', 'newFlag', 'popularFlag'],
      where: { 'quantity': { '>': 0 } },
    });
    const allNomenclatures = await NomenclaturesOfGoods.find({
      select: ['id', 'name'],
    });
    const newOrders = await Orders.find({
      where: { 'statusCode': 0 },
    });
    let tmp;
    allGoods.map((el) => {
      tmp = allNomenclatures.filter(nomenclature => nomenclature.id === el.nomenclatureId)[0];
      el.name = tmp.name;
    });
    const salesList = allGoods.filter(el => el.salesFlag);
    const newList = allGoods.filter(el => el.newFlag);
    const popularList = allGoods.filter(el => el.popularFlag);
    const configList = { salesList, newList, popularList };
    return res.view('dashboard', {
      configList,
      allGoods,
      users: { quantity: usersQuantity },
      blog: { quantity: articlesQuantity },
      goods: { allQuantity: goodsAllQuantity, quantity: goodsQuantity },
      orders: {
        qtyOpened: openedOrders.length,
        qtyProcessed: processedOrders.length,
        qtyClosed: closedOrders.length,
      },
      areNewOrders: newOrders.length !== 0,
    });
  },
  async refreshHomepageLists(req, res) {
    const resetAll = await Goods.update({
      or: [{ 'salesFlag': 1 }, { 'popularFlag': 1 }, { 'newFlag': 1 }],
    }).set({ 'salesFlag': 0, 'popularFlag': 0, 'newFlag': 0 });

    if (!req.body.configList) return res.send('Homepage list successfully changed.');
    const salesList = req.body.configList.salesList ?
      req.body.configList.salesList.map(el => parseInt(el)) : false;
    if (salesList) {
      await Goods.update({
        'id': salesList,
      }).set({
        'salesFlag': 1,
      });
    }
    const popularList = req.body.configList.popularList ?
      req.body.configList.popularList.map(el => parseInt(el)) : false;
    if (popularList) {
      await Goods.update({
        'id': popularList,
      }).set({
        'popularFlag': 1,
      });
    }
    const newList = req.body.configList.newList ?
      req.body.configList.newList.map(el => parseInt(el)) : false;
    if (newList) await Goods.update({ 'id': newList }).set({ 'newFlag': 1 });
    return res.send('Homepage list successfully changed.');
  },
};
