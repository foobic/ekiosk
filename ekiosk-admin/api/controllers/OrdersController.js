/**
 * OrdersController
 *
 * @description :: Server-side logic for managing Orders
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const CommonServices = require('../services/CommonServices');

module.exports = {
  // show /categories/showAll  page
  async showAll(req, res) {
    let showType = Object.keys(req.query)[0]; // all, opened, processed, closed
    let showTypeArray = null;
    if (!(showType === 'opened' || showType === 'processed' || showType === 'closed')) {
      showType = 'all';
    }
    switch (showType) {
      case 'all':
        showTypeArray = [0, 1, 2];
        break;
      case 'opened':
        showTypeArray = [0];
        break;
      case 'processed':
        showTypeArray = [1];
        break;
      case 'closed':
        showTypeArray = [2];
        break;
    }
    let orders = await Orders.find({
      where: { statusCode: showTypeArray },
    });
    // orders.map((el) => {
    //   console.log(el.createdAt);
    // });

    const getnormdate = date => date.toISOString().slice(0, 10).split('-').join('/');
    // if (req.query.date) {
    //   let currentDate = new Date(req.query.date);
    //   currentDate.setDate(currentDate.getDate() + 1);
    //   currentDate = getnormdate(currentDate);
    //   orders = orders.filter(order => getnormdate(order.createdAt) === currentDate);
    if (req.query.startDate && req.query.endDate) {
      const startDate = new Date(req.query.startDate);
      // startDate.setDate(startDate.getDate() + 1);
      const endDate = new Date(req.query.endDate);
      endDate.setDate(endDate.getDate() + 1);
      orders = orders.filter(order => order.createdAt >= startDate && order.createdAt <= endDate);
    }
    // console.log(orders);
    for (let i = 0; i < orders.length; i++) {
      // converting bytes to utf8
      orders[i].items = CommonServices
        .stringFromUTF8Array(orders[i].items);
      orders[i].createdAt = CommonServices.convertDate(orders[i].createdAt);
    }
    const users = await Users.find({});
    orders = orders.map((order) => {
      order.customer = users.filter(el => el.id === order.customer)[0];
      return order;
    });
    return res.view('orders/all', {
      orders,
      showType: showType.toString(),
    });
  },
  async showOne(req, res) {
    // console.log(req.param('id'));
    // console.log(typeof req.param('id'));
    const id = req.param('id');
    const order = await Orders.findOne({ where: { id } });
    if (order == null) {
      return res.view('alerts/error', { alertMsg: 'order not found' });
    } else {
      order.items = JSON.parse(CommonServices
        .stringFromUTF8Array(order.items));
      order.createdAt = CommonServices.convertDate(order.createdAt);
      const goodsIds = order.items.map(el => parseInt(el.id));
      const goodsByIds = await Goods.find({ where: { id: goodsIds } });
      const nomenclaturesIds = goodsByIds.map(el => parseInt(el.nomenclatureId));
      const nomenclaturesByIds = await NomenclaturesOfGoods.find({
        where: { id: nomenclaturesIds },
      });
      let tmp;
      let profit = 0;
      let allqty = 0;

      // const users = await Users.find({});
      order.customer = await Users.findOne({ where: { id: order.customer } });
      // console.log(goodsIds.length);
      // console.log(goodsByIds.length);
      order.items.map((el) => {
        tmp = goodsByIds.filter(element => element.id == el.id)[0];
        // console.log('**', tmp);
        el.realPrice = tmp.realPrice;
        el.nomenclatureId = tmp.nomenclatureId;
        tmp = nomenclaturesByIds.filter(ele => ele.id == el.nomenclatureId)[0];
        el.name = tmp.name;
        el.price = parseInt(el.price);
        el.quantity = parseInt(el.quantity);
        profit += (el.price - el.realPrice) * el.quantity;
        allqty += el.quantity;
      });
      order.profit = profit;
      order.allqty = allqty;
      // console.log(order.items);
      // console.log('->', goodsByIds, '<-');
      return res.view('orders/one', { order });
    }
  },
  async editOne(req, res) {
    const id = req.param('id');
    await Orders.update({ id }).set({
      statusCode: parseInt(req.param('statusCode')),
      orderDesc: req.param('orderDesc'),
    }).then(() =>
      res.redirect('/orders/editOne/' + id)
    );
  },
  async deleteOne(req, res) {
    // console.log( parseInt(req.param('id')) );
    Orders.destroy({
      id: parseInt(req.param('id')),
    }, (err) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Order successfully destroyed');
      }
    });
  },

};

