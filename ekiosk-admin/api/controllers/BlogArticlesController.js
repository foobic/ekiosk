/**
 * BlogArticlesController
 *
 * @description :: Server-side logic for managing BlogArticless
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const BlogArticlesService = require('../services/BlogService.js');

module.exports = {

  async showAll(req, res) {
    await BlogArticles.find({
      sort: 'id desc',
    }, (err, articles) => {
      if (err) {
        return res.serverError(err);
      }
      for (let i = 0; i < articles.length; i++) {
        articles[i].body = BlogArticlesService
          .stringFromUTF8Array(articles[i].body)
          .substring(0, 50) + '...';
      }
      return res.view('blog/list', {
        data: articles,
      });
    });
  },

  async showEdit(req, res) {
    await BlogArticles.findOne({
      where: {
        id: req.param('id'),
      },
    }, (err, article) => {
      if (err) {
        return res.serverError(err);
      } else if (article === undefined) {
        return res.view('alerts/error', {
          alertMsg: 'Wrong article url.',
        });
      } else {
        return res.view('blog/editOne', {
          data: article,
        });
      }
    });
    // return res.view('blog/list', {data: articles});
  },

  async editOne(req, res) {
    await BlogArticles.update({
      id: req.param('id'),
    }).set({ title: req.param('title'), body: req.param('body') });
    // console.log('/body/edit/' + req.param('id'));
    return res.redirect('/blog/editOne/' + req.param('id'));
  },

  async addOne(req, res) {
    await BlogArticles.create({
      title: req.param('title'),
      body: req.param('body'),
    }, () => res.view('alerts/success', {
      alertMsg: 'article successfully added',
    }));
  },
};
