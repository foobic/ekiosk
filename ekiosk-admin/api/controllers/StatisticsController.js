/**
 * StatisticsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const CommonServices = require('../services/CommonServices');
module.exports = {

  // show some data
  async index(req, res) {
    const usersQuantity = await Users.count({});
    const articlesQuantity = await BlogArticles.count({});
    const goodsAllQuantity = await Goods.count({});
    const goodsQuantity = await Goods.count({
      where: { 'quantity': { '>': 0 } },
    });
    const allOrders = await Orders.find({ });

    const openedOrders = allOrders.filter(order => order.statusCode === 0);
    const processedOrders = allOrders.filter(order => order.statusCode === 1);
    const closedOrders = allOrders.filter(order => order.statusCode === 2);

    return res.view('statistics', {
      summary: { proceeds: 1, profit: 1 },
      users: { quantity: usersQuantity },
      blog: { quantity: articlesQuantity },
      goods: { allQuantity: goodsAllQuantity, quantity: goodsQuantity },
      orders: {
        qtyOpened: openedOrders.length,
        qtyProcessed: processedOrders.length,
        qtyClosed: closedOrders.length,
      },
    });
  },
  async getReportByDateRange(req, res) {
    const daterange = req.body.daterange;
    const allOrders = await Orders.find({ });
    let closedOrders = allOrders.filter(order => order.statusCode === 2);
    const startDate = new Date(daterange.fromRaw);
    const endDate = new Date(daterange.toRaw);
    endDate.setDate(endDate.getDate() + 1);
    closedOrders = closedOrders.filter(order => order.createdAt >= startDate && order.createdAt <= endDate);
    const goodIds = [];
    closedOrders = closedOrders.map((el) => {
      el.items = CommonServices.stringFromUTF8Array(el.items);
      el.items = JSON.parse(el.items);
      el.items.map((item) => {
        goodIds.push(parseInt(item.id));
      });
      return el;
    });
    const goodsList = await Goods.find({ id: goodIds });
    const nomenclatures = await NomenclaturesOfGoods.find({});
    const counts = {};
    goodIds.forEach((x) => {
      counts[x] = (counts[x] || 0) + 1;
    });
    let goods = [];
    for (const key in counts) {
      counts[key] = { goodId: parseInt(key), qty: parseInt(counts[key]) };
      goods.push(counts[key]);
    }
    let tmp;
    const total = { qty: 0, sum: 0, profit: 0 };
    goods = goods.map((good) => {
      tmp = goodsList.filter(x => x.id === good.goodId)[0];
      good.nomenclatureId = tmp.nomenclatureId;
      good.price = tmp.price;
      good.realPrice = tmp.realPrice;
      tmp = nomenclatures.filter(x => x.id === good.nomenclatureId)[0];
      good.nomenclatureName = tmp.name;
      good.sum = good.price * good.qty;
      good.profit = (good.price - good.realPrice) * good.qty;
      total.qty += good.qty;
      total.sum += good.sum;
      total.profit += good.profit;
      return good;
    });
    const sortField = req.body.search.sortBy;
    if (req.body.search.sortDirection === 'asc') {
      goods.sort((x, y) => x[sortField] > y[sortField]);
    } else {
      goods.sort((x, y) => x[sortField] < y[sortField]);
    }
    console.log(goods);
    console.log(total);
    return res.json({
      total,
      avgSum: Math.round(total.sum / closedOrders.length, 4),
      closedOrdersQty: closedOrders.length,
      goods,
    });
  },

};

