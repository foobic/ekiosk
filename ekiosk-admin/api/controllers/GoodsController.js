/**
 * GoodsController
 *
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// const mkdirp = require('mkdirp');
// const fs = require('fs');
// const path = require('path');
// const CommonServices = require('../services/CommonServices');

// const fsImagesDir = 'assets/images/products/';
// const dbImagesDir = '/images/products/';

module.exports = {
  // show all goods
  async showAll(req, res) {
    // getting all goods
    let queryString = { 'where': { 'quantity': { '>': 0 } } };
    if (req.query.all === '') {
      queryString = {};
    } else if (req.query.archived === '') {
      queryString = { 'where': { 'quantity':  0 } };
    }
    const nomenclatures = await NomenclaturesOfGoods.find({});
    const providers = await Providers.find({});
    const delivers = await Delivers.find({});
    let goods = await Goods.find(queryString);
    goods = goods.map((good) => {
      good.nomenclature = nomenclatures.filter(el => el.id === good.nomenclatureId)[0];
      return good;
    });
    goods = goods.map((good) => {
      good.provider = providers.filter(el => el.id === good.providerId)[0];
      return good;
    });
    goods = goods.map((good) => {
      good.deliver = delivers.filter(el => el.id === good.deliverId)[0];
      return good;
    });
    return res.view('goods/list', {
      data: goods,
    });
  },
  // show /goods/addOne page
  async showAddOne(req, res) {
    const providers = await Providers.find({});
    const delivers = await Delivers.find({});
    await NomenclaturesOfGoods.find({
      sort: 'categoryId asc',
    }, (err, nomenclatures) => {
      if (nomenclatures.length === 0 || providers.length === 0 || delivers.length === 0) {
        return res.view('alerts/error', {
          alertMsg: 'Create nomenclature and provider and deliver first',
        });
      } else {
        return res.view('goods/addOne', {
          nomenclatures,
          providers,
          delivers,
          nomenclature: nomenclatures[0],
          provider: providers[0],
          deliver: delivers[0],
        });
      }
    });
  },
  // add one good functionality
  async addOne(req, res) {
    // create request with params which was recieved
    // from view by by POST request
    await Goods.create({
      // name: req.param('name'),
      // desc: req.param('desc'),
      // categoryId: parseInt(req.param('catId')),
      nomenclatureId: parseInt(req.param('nomenclatureId')),
      quantity: parseInt(req.param('quantity')),
      // manufactor: req.param('manufactor'),
      price: parseFloat(req.param('price')),
      discount: parseFloat(req.param('discount')),
      deliverId: parseFloat(req.param('deliverId')),
      providerId: parseFloat(req.param('providerId')),
      realPrice: parseFloat(req.param('realPrice')),
      // images: JSON.stringify({}),
      // properties: JSON.stringify({}),
      // customProperties: JSON.stringify({}),
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'product successfully created',
      });
    });
  },
  // show /goods/editOne page
  async showEditOne(req, res) {
    const nomenclatures = await NomenclaturesOfGoods.find();
    const providers = await Providers.find({});
    const delivers = await Delivers.find({});
    // console.log(providers);

    await Goods.findOne({
      where: { id: req.param('id') },
    }, (err, good) => {
      // if find nothing return alert
      if (!good) {
        return res.view('alerts/error', {
          alertMsg: 'Product doesn\'t exists',
        });
      } else {
        const nomenclature = nomenclatures.filter(obj => obj.id === good.nomenclatureId)[0];
        // console.log(nomenclature);
        // good.images = nomenclature.images;
        // convert bytes to str
        // good.images = CommonServices.stringFromUTF8Array(good.images);
        // console.log(good);
        // console.log(providers.filter((obj) => {
        //   console.log(obj);
        //   return obj.id === good.providerId;
        // }));
        return res.view('goods/editOne', {
          good,
          nomenclatures,
          nomenclature,
          providers,
          provider: providers.filter(obj => obj.id === good.providerId)[0],
          delivers,
          deliver: delivers.filter(obj => obj.id === good.deliverId)[0],
          // nomenclature: nomenclatures[0],
        });
      }
    });
  },
  // edit one good functionality
  async editOne(req, res) {
    const id = req.param('id');
    const nomenclatureId = req.param('id');
    await NomenclaturesOfGoods.find({
      sort: 'categoryId asc',
    }, (err, nomenclatures) => {
      const nomenclature = nomenclatures.filter(nomen => nomen.id === parseInt(nomenclatureId))[0];
      // res.view('goods/addOne', {
      //   nomenclatures,
      //   nomenclature,
      // });
    });
    await Goods.update({ id }).set({
      // name: req.param('name'),
      // desc: req.param('desc'),
      // categoryId: parseInt(req.param('catId')),
      nomenclatureId: parseInt(req.param('nomenclatureId')),
      quantity: parseInt(req.param('quantity')),
      // manufactor: req.param('manufactor'),
      price: parseFloat(req.param('price')),
      discount: parseFloat(req.param('discount')),
      deliverId: parseFloat(req.param('deliverId')),
      providerId: parseFloat(req.param('providerId')),
      realPrice: parseFloat(req.param('realPrice')),
      // images: JSON.stringify(images.concat(JSON.parse(existsRecord.images))),
      // properties: JSON.stringify({}),
      // customProperties: JSON.stringify({}),
    }).then(() =>
    // then -> redirect back
      res.redirect('/goods/editOne/' + id)
    );
  },

  async deleteOne(req, res) {
    // console.log( parseInt(req.param('id')) );
    Goods.destroy({
      id: parseInt(req.param('id')),
    }, (err) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Order successfully destroyed');
      }
    });
  },


};
