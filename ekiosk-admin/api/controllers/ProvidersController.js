/**
 * ProvidersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  // showAll
  async showAll(req, res) {
    await Providers.find({ }, (err, providers) => {
      if (err) {
        return res.serverError(err);
      } else {
        // return results to view
        return res.view('providers/list', {
          data: providers,
        });
      }
    });
  },
  // showAddOne
  async showAddOne(req, res) {
    return res.view('providers/addOne');
  },

  // addOne
  async addOne(req, res) {
    // just create request with params which recieved from view
    await Providers.create({
      name: req.param('name'),
    }, (err) => {
      if (err) {
        return res.serverError(err);
      }
      return res.view('alerts/success', {
        alertMsg: 'Provider successfully created',
      });
    });
  },
  // showEditOne
  async showEditOne(req, res) {
    await Providers.findOne({
      where: { id: req.param('id') },
    }, (err, provider) => {
      if (err) {
        return res.serverError(err);
      }
      if (provider === undefined) {
        return res.view('alerts/error', {
          alertMsg: 'Wrong provider edit url.',
        });
      }
        return res.view('providers/editOne',{
            data: provider
        });
    });
  },
  // editOne
  async editOne(req, res) {
    // just update request to db wia WaterLine
    await Providers.update({
      id: req.param('id'),
    }).set({
      name: req.param('name'),
    });
    return res.redirect('/providers/editOne/' + req.param('id'));
  },

  async deleteOne(req, res) {
    Providers.destroy({
      id: parseInt(req.param('id')),
    }, (err, destroyedProvider) => {
      if (err) {
        res.json(500, { error: err.message });
      } else {
        res.send('Provider successfully destroyed');
      }
    });
  },
};

