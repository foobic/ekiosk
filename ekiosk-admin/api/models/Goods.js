/**
 * Goods.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const NotImplementedError = require('../services/notImplementedError.js');

module.exports = {
  tableName: 'Goods',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    quantity: {
      type: 'integer',
      required: true,
    },
    discount: {
      type: 'integer',
      required: true,
    },
    price: {
      type: 'float',
      required: true,
    },
    realPrice: { // just entry price
      type: 'float',
      required: true,
      defaultsTo: 0.0,
    },
    salesFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    newFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    popularFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    nomenclatureId: {
      model: 'NomenclaturesOfGoods',
    },
    providerId: {
      model: 'Providers',
    },
    deliverId: {
      model: 'Delivers',
    },
  },
  async beforeUpdate(criteria, proceed) {
    console.log('hi from goods');
    return proceed();
  },
  async beforeDestroy(criteria, proceed) {
    const deleteRowId = criteria.where.id;
    await Orders.find({
    }, async(err, rows) => {
      if (err) {
        return proceed(new NotImplementedError('undefined error'));
      } else {
        let curItem = null;
        let stop = false;
        for (let i = 0, len = rows.length; i < len; i++) {
          curItem = JSON.parse(rows[i].items);
          for (let j = 0, lenj = curItem.length; j < lenj; j++) {
            if (parseInt(curItem[j].id) === deleteRowId) {
              // console.log('end');
              stop = true;
              break;
            }
          }
          if (stop) break;
        }
        if (stop) {
          return proceed(new NotImplementedError('ERR: you could not ' +
                 'delete good while it has references from Orders '));
        } else {
          return proceed();
        }
      }
    });

    // await Goods.findOne({
    //   where: { id: deleteRowId },
    // }, async(err, deletedRow) => {
    //   if (err) {
    //     return proceed(new NotImplementedError('undefined error'));
    //   } else {
    //     const deleteNumID = deletedRow.nomenclatureId;
    //     await NomenclaturesOfGoods.findOne({
    //       where: { id: deleteNumID },
    //     }, (err, deleteNomenclature) => {
    //       if (err) {
    //         return proceed(new NotImplementedError('undefined error'));
    //       else if (deleteNomenclature) {
    //         return proceed(new NotImplementedError('ERR: you could not ' +
    //              'delete good while it has references from NomenclaturesOfGoods '));
    //       } else {
    //         return proceed();
    //       }
    //     });
    //   }
    // });
  },
};

