/**
 * Categories.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const NotImplementedError = require('../services/notImplementedError.js');
module.exports = {
  tableName: 'Categories',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true,
      maxLength: 255,
    },
    parentId: {
      type: 'integer',
      required: true,
    },
    filters: {
      type: 'binary',
      // required: true,
    },

    // reference to Goods
    goods: {
      collection: 'NomenclaturesOfGoods',
      via: 'categoryId',
    },

  },
  async beforeDestroy(criteria, proceed) {
    const deleteCategory = criteria.where.id;
    await Categories.findOne({
      where: { id: deleteCategory },
    }, (err, category) => {
      // if category exitst
      if (category) {
        // find this category as parent category
        Categories.findOne({
          where: { parentId: deleteCategory },
        }, (err, parentCategory) => {
          // if some categories has this category as a parent return Error
          console.log(parentCategory);
          if (parentCategory) {
            return proceed(
              new NotImplementedError('ERR: you could not delete category ' +
                    'if another category has it as parent category')
            );
          } else {
            // if not check if this category has goods
            NomenclaturesOfGoods.find({
              where: { categoryId: deleteCategory },
            }, (err, goods) => {
            // if some goods belongs to this category -> return Error
              console.log(goods);
              console.log(goods.length !== 0);
              if (err) {
                return proceed(new NotImplementedError('undefined error'));
              }
              // console.log(goods.length !== 0);
              if (goods.length !== 0) {
                return proceed(new NotImplementedError('ERR: you could not ' +
                 'delete category while nomenclature of good references to this category'));
                // if not. delete this category
              } else {
                return proceed();
              }
            });
          }
        });
        // if category non exits
      } else {
        return proceed(new NotImplementedError('ERR: undefined category'));
      }
    });
  },
};

