/**
 * Account.js
 *
 * @description :: TODO: You might write a short summary of how
 * this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'TempUsers',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true,
      maxLength: 30,
    },
    email: {
      type: 'string',
      required: true,
      maxLength: 60,
    },
    verifyKey: {
      type: 'string',
      required: true,
      maxLength: 255,
    },
    password: {
      type: 'string',
      required: true,
      minLength: 6,
    },
    lastEnter: {
      type: 'date',
      defaultsTo() {
        return new Date();
      },
    },
    verifyPassword(password) {
      return bcrypt.compareSync(password, this.password);
    },
    verifyPassword(password) {
      return bcrypt.compareSync(password, this.password);
    },
    changePassword(newPassword, cb) {
      this.newPassword = newPassword;
      this.save((err, u) => cb(err, u));
    },
    toJSON() {
      return this.toObject();
    },
  },

  beforeCreate(attrs, cb) {
    bcrypt.hash(attrs.password, SALT_WORK_FACTOR, (err, hash) => {
      attrs.password = hash;
      return cb();
    });
  },

  beforeUpdate(attrs, cb) {
    if (attrs.newPassword) {
      bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) return cb(err);
        bcrypt.hash(attrs.newPassword, salt, (err, crypted) => {
          if (err) return cb(err);
          delete attrs.newPassword;
          attrs.password = crypted;

          return cb();
        });
      });
    } else {
      return cb();
    }
  },
};
