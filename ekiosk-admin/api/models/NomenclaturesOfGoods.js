/**
 * NomenclaturesOfGoods.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const NotImplementedError = require('../services/notImplementedError.js');

module.exports = {

  tableName: 'NomenclaturesOfGoods',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true,
      maxLength: 255,
    },
    desc: {
      type: 'string',
      // required: true,
      maxLength: 1000,
    },
    manufactor: {
      type: 'string',
      // required: true,
      maxLength: 1000,
    },
    categoryId: {
      type: 'integer',
      required: true,
      model: 'Categories',
    },
    properties: {
      type: 'binary',
      // required: true,
    },
    customProperties: {
      type: 'binary',
      // required: true,
    },
    images: {
      type: 'binary',
      // required: true,
    },
    goods: {
      collection: 'goods',
      via: 'nomenclatureId',
    },
  },
  async beforeDestroy(criteria, proceed) {
    const deleteRowId = criteria.where.id;
    await Goods.findOne({
      where: { nomenclatureId: deleteRowId },
    }, async(err, deletedRow) => {
      if (err) {
        return proceed(new NotImplementedError('undefined error'));
      } else if (deletedRow) {
        return proceed(new NotImplementedError('ERR: you could not ' +
             'delete nomenclature while it has references from Goods '));
      } else {
        return proceed();
      }
    });
  },

};

