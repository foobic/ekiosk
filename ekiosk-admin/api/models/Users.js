/**
 * Account.js
 *
 * @description :: TODO: You might write a short summary of how
 * this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const NotImplementedError = require('../services/notImplementedError.js');
module.exports = {
  tableName: 'Users',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    userDesc: {
      type: 'string',
      maxLength: 3000,
    },
    name: {
      type: 'string',
      required: true,
      maxLength: 30,
    },
    phone: {
      type: 'string',
      required: true,
      maxLength: 19,
      minLength: 19,
    },
    email: {
      type: 'string',
    },
    // password: {
    //   type: 'string',
    //   minLength: 6,
    // },
    // lastEnter: {
    //   type: 'date',
    //   defaultsTo() {
    //     return new Date();
    //   },
    // },
    verifyPassword(password) {
      return bcrypt.compareSync(password, this.password);
    },
    changePassword(newPassword, cb) {
      this.newPassword = newPassword;
      this.save((err, u) => cb(err, u));
    },
    toJSON() {
      return this.toObject();
    },
  },

  beforeCreate(attrs, cb) {
    bcrypt.hash(attrs.password, SALT_WORK_FACTOR, (err, hash) => {
      attrs.password = hash;
      return cb();
    });
  },

  beforeUpdate(attrs, cb) {
    if (attrs.newPassword) {
      bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) return cb(err);
        bcrypt.hash(attrs.newPassword, salt, (err, crypted) => {
          if (err) return cb(err);
          delete attrs.newPassword;
          attrs.password = crypted;

          return cb();
        });
      });
    } else {
      return cb();
    }
  },
  async beforeDestroy(criteria, proceed) {
    const customerId = criteria.where.id;
    console.log(customerId);
    await Orders.find({
      where: { customer: customerId },
    }, async(err, deletedRow) => {
      if (err) {
        return proceed(new NotImplementedError('undefined error'));
      } else if (deletedRow.length != 0) {
        return proceed(new NotImplementedError('ERR: you could not ' +
             'delete user if he made at least one order '));
      } else {
        return proceed();
      }
    });
  },

};
