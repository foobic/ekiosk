/**
 * Orders.js
 *
 * @description :: TODO: You might write a short summary of how this 
 * model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'Orders',
  connection: 'PostgresqlServerAdmin',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    customer: {
      type: 'integer',
      required: true,
    },
    orderDesc: {
      type: 'string',
      required: true,
      maxLength: 3000,
      defaultsTo: '-',
    },
    items: {
      type: 'binary',
      required: true,
    },
    sum: {
      type: 'float',
      required: true,
    },
    statusCode: {
      type: 'integer',
      required: true,
      defaultsTo: 0,
    },
  },
};

