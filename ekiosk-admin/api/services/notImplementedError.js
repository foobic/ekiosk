/* Goods*
 *  * Sets no-cache header in response.
 *   */
const NotImplementedError = function(message) {
  this.name = 'NotImplementedError';
  this.message = (message || '');
};

NotImplementedError.prototype = Error.prototype;

module.exports = NotImplementedError;
