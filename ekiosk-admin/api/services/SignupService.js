
const SignupService = {
  getVerifyKey() {
    let text = '';
    const possible =
          'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 15; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  },
  async emailExists(table, email) {
    // if email already used
    return await table.findOne({
      email,
    }).then((emailObj) => {
      if (typeof emailObj !== 'undefined') {
        return true;
      }
      return false;
    });
  },
};


module.exports = SignupService;
