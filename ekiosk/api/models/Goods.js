/**
 * @module Goods
 * @category models
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'Goods',
  connection: 'PostgresqlServer',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    quantity: {
      type: 'integer',
      required: true,
    },
    discount: {
      type: 'integer',
      required: true,
    },
    price: {
      type: 'float',
      required: true,
    },
    realPrice: {
      type: 'float',
      required: true,
      defaultsTo: 0.0,
    },
    salesFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    newFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    popularFlag: {
      type: 'integer',
      defaultsTo: 0,
    },
    nomenclatureId: {
      model: 'NomenclaturesOfGoods',
    },
    providerId: {
      model: 'Providers',
    },
    deliverId: {
      model: 'Delivers',
    },
  },
  // async beforeUpdate(criteria, proceed) {
    // console.log('hi from goods');
    //   console.log(criteria);
    // return proceed();
  // },
};

