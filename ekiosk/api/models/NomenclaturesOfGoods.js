/**
 * @module NomenclaturesOfGoods
 * @category models
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'NomenclaturesOfGoods',
  connection: 'PostgresqlServer',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true,
      maxLength: 255,
    },
    desc: {
      type: 'string',
      // required: true,
      maxLength: 1000,
    },
    manufactor: {
      type: 'string',
      // required: true,
      maxLength: 1000,
    },
    categoryId: {
      type: 'integer',
      required: true,
      model: 'Categories'
    },
    properties: {
      type: 'binary',
      // required: true,
    },
    customProperties: {
      type: 'binary',
      // required: true,
    },
    images: {
      type: 'binary',
      // required: true,
    },
    goods: {
      collection: 'goods',
      via: 'nomenclatureId',
    },
  },

};

