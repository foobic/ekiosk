/**
 * @module Providers
 * @category models
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'Providers',
  connection: 'PostgresqlServer',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true,
    },
    goods: {
      collection: 'goods',
      via: 'providerId',
    },
  },

};

