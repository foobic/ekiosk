/**
 * @module BlogArticles
 * @category models
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this
 * model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'BlogArticles',
  connection: 'PostgresqlServer',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: 'string',
      required: true,
      maxLength: 255,
    },
    body: {
      type: 'binary',
      required: true,
    },
  },
};

