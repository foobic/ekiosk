/**
 * @module Orders
 * @category models
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

const CommonService = require('../services/CommonServices.js');
const NotImplementedError = require('../services/notImplementedError.js');
module.exports = {

  tableName: 'Orders',
  connection: 'PostgresqlServer',

  attributes: {
    id: {
      type: 'integer',
      primaryKey: true,
      autoIncrement: true,
    },
    customer: {
      type: 'integer',
      required: true,
    },
    orderDesc: {
      type: 'string',
      required: true,
      maxLength: 3000,
       defaultsTo: '-'
    },
    items: {
      type: 'binary',
      required: true,
    },
    sum: {
      type: 'float',
      required: true,
    },
    statusCode: {
      type: 'integer',
      required: true,
       defaultsTo: 0
    },
  },
  async beforeCreate(criteria, proceed) {
    const decodedItems = JSON.parse(CommonService.stringFromUTF8Array(criteria.items)).filter((obj) => {
      obj.id = parseInt(obj.id);
      obj.quantity = parseInt(obj.quantity);
      obj.price = parseFloat(obj.price);
      return obj;
    });
    const decodedIds = decodedItems.map(obj => obj.id);
    await Goods.find({
      id: decodedIds,
    }).then(async(goods, error) => {
      if (error || goods.length === 0 ) {
        return proceed(new NotImplementedError('Unhandled error'));
      } else if( goods.length != decodedIds.length){
        return proceed(new NotImplementedError('Product(s) was deleted'));
      }else {
        let errorFlag = false;
        let errWhileSaving = false;
        let itemsQtys = []
        let item;
        for (let i = 0; i < goods.length; i++) {
          item = decodedItems.filter(obj => obj.id == goods[i].id)[0];
          if (item.quantity > goods[i].quantity) {
            errorFlag = true;
            console.log(goods[i]);
            itemsQtys.push({id: goods[i].id, qty: goods[i].quantity})
          }
        }
        if (errorFlag) {
          console.log(itemsQtys);
          return proceed(new NotImplementedError('Not in Stock. The max values have been set.',itemsQtys));
        } else {
          for (let i = 0; i < goods.length; i++) {
            item = decodedItems.filter(obj => obj.id == goods[i].id)[0];
            goods[i].quantity -= item.quantity;
            goods[i].save((error) => {
              if (error) {
                errWhileSaving = true;
              }
            });
          }
        }
        if (errWhileSaving) {
          return proceed(new NotImplementedError('Unhandled error'));
        } else {
          return proceed();
        }
      }
    });
  },
};

