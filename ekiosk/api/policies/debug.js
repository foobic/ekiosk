/**
 * @module Debug
 * @category policies
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description needs for debug
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
module.exports = async function(req, res, proceed) {
  // console.log(req);
  console.log('=============REQUEST================');
  console.log('Title: ', req.title);
  console.log('Path: ', req.path);
  console.log('Lang: ', req.language);
  console.log('Locale: ', req.locale);
  console.log('Cookies: ', req.cookies);
  console.log('SessionID: ', req.sessionID);
  console.log('Session: ', req.session);

  return proceed();
};
