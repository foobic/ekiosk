/**
 * @module isAuth
 * @category policies
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description if user auth;
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = async function(req, res, proceed) {
  if (req.session.auth) {
    return proceed();
  }
    console.log('hello from auth')

  return res.redirect('/login');
};
