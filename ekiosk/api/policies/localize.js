/**
 * @module localize
 * @category policies
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description policy for localization
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
module.exports = async function(req, res, proceed) {

  // req.locale = 'es';
  // req.language = 'es';
  return proceed();
};
