/**
 * @module resetBasket
 * @category policies
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description policy for reseting basket
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = async function(req, res, proceed) {
  console.log('====');
  console.log(req.session);
  if (req.session.basket == null && req.session.basketData == null) {
    // req.session.basket = [];
    // req.session.basketData = {"qty":0 , "sum": 0}
    return proceed();
    // console.log(JSON.stringify( req.cookies ))
    // console.log(JSON.stringify( req.session ))
    // return res.redirect('/');
  } else {
    return proceed();
  }

  // return res.redirect('/login');
};
