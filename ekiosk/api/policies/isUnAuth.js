/**
 * @module isUnAuth
 * @category policies
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description if user unauth;
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = async function(req, res, proceed) {
  if (!req.session.auth) {
    return proceed();
  }

  return res.redirect('/account');
};
