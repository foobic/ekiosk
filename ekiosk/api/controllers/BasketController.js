/**
 * @module BasketController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description ::Server-side logic for managing Baskets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description makes new order */
  async checkout(req, res) {
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // TRANSACTION NEEDED
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if (req.xhr) {
      // items
      const pureItems = req.body.order.items;
      const items = new Buffer(JSON.stringify(pureItems), 'utf-8');
      const userData = req.body.userData;
      let customerId;
      const userExists = await Users.findOne({
        where: { 'phone': userData.phone },
      });
      if (!userExists) {
        const newUser = await Users.create({
          phone: userData.phone,
          name: userData.name,
          email: userData.email,
        });
        customerId = newUser.id;
      } else {
        customerId = userExists.id;
      }
      await Orders.create({
        customer: parseInt(customerId),
        items,
        sum: parseFloat(req.body.order.sum).toFixed(2),
      }, async(err, ok) => {
        if (err) {
          await Users.destroy({
            id: customerId,
          });
          return res.json(500, { error: err.message, data: err.data });
        } else {
          req.session.basket = [];
          req.session.basketData = { qty: 0, sum: 0 };
          return res.send();
        }
      });
    }
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description just show a basket*/
  async show(req, res) {
    // console.log('----------------->', req.session.userID);
    return res.view('basket', {
      title: 'Basket',
      basket: req.session.basket,
      basketData: req.session.basketData,
      userID: req.session.userID == null ? 0 : req.session.userID,
    });
  },

  async refreshBasket(req, res) {
    req.body.basketData.qty = parseInt(req.body.basketData.qty);
    req.body.basketData.sum = parseFloat(req.body.basketData.sum);

    if (req.body.basket) {
      for (let i = 0, len = req.body.basket.length; i < len; i++) {
        req.body.basket[i].id = parseInt(req.body.basket[i].id);
        req.body.basket[i].qty = parseInt(req.body.basket[i].qty);
        req.body.basket[i].price = parseFloat(req.body.basket[i].price);
        // console.log(req.body.basket[i].price);
        // console.log(typeof(req.body.basket[i].price));
      }
    }

    req.session.basketData = req.body.basketData;
    req.session.basket = req.body.basket ? req.body.basket : [];
    res.send();
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description delete one item from cart*/
  async deleteOne(req, res) {
    let item;
    for (let i = 0; i < req.session.basket.length; i++) {
      if (req.session.basket[i].id == req.body.id) {
        item = req.session.basket[i];
      }
    }
    const index = req.session.basket.indexOf(item);
    if (index > -1) {
      req.session.basketData.qty -= item.qty;
      req.session.basketData.sum = parseFloat(req.session.basketData.sum - item.price * item.qty).toFixed(2);
      req.session.basket.splice(index, 1);
      return res.send();
    } else {
      return res.json(500, { error: 'error while deleting' });
    }
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description add items to cart*/
  async add(req, res) {
    if (req.xhr) {
      const data = req.body;
      const id = parseInt(data.id);
      const price = parseFloat(parseFloat(data.price).toFixed(2));
      const name = data.name;
      const qty = parseInt(data.qty);

      // const print = (obj) => {
      // console.log(obj, typeof obj);
      // };


      // console.log('backend', req.session.basket);
      if (req.session.basket == null) {
        req.session.basket = [{
          id,
          price,
          name,
          qty,
        }];
        req.session.basketData = { 'qty': 1, 'sum': price };
      } else {
        // if item already exitst in basket->increment qty
        let flag = true;
        for (let i = 0; i < req.session.basket.length; i++) {
          if (req.session.basket[i].id == id) {
            req.session.basket[i].qty = req.session.basket[i].qty + 1;
            flag = false;
            break;
          }
        }
        if (flag) {
          // else - add it
          req.session.basket.push({
            id,
            price,
            name,
            qty,
          });
        }
        req.session.basketData.qty = req.session.basketData.qty + 1;
        req.session.basketData.sum = (parseFloat(req.session.basketData.sum) + parseFloat(price)).toFixed(2);
      }
      // console.log('backend after');
      // console.log(req.session.basket);
      req.session.save(() => {
        // console.log('saved');
      });
      res.send();
    }
  },
};

