/**
 * @module  BlogArticlesController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing BlogArticless
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


const BlogArticlesService = require('../services/BlogService.js');

module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description show all articles*/
  async showAll(req, res) {
    let skip = 0;
    const limit = 5;
    const pageQuantity = Math.ceil(await BlogArticles.count({}) / limit);
    let page = 1;
    try {
      // if url /blog or /blog/
      if (req.param('page') === undefined) {
        throw new Error('oops');
      }
      // // if url contain page number
      page = Number(req.param('page'));
      if (page !== undefined && !isNaN(page)) {
        skip = limit * (page - 1);
      }
    } catch (err) {
      // console.log('Page wrong');
    }

    await BlogArticles.find({
      sort: { createdAt: 'desc' },
      where: { skip, limit },
    }, (err, articles) => {
      if (err) {
        return res.serverError(err);
      }
      // if articles quantity = 0
      if (pageQuantity === 0) {
        return res.view('alerts/error', {
          title: 'Ошибка',
          alertMsg: 'Статей еще нет.',
        });
      }
      // if find method returns empty array
      if (articles.length === 0) {
        return res.view('alerts/error', {
          title: 'Ошибка',
          alertMsg: 'Статей не найдено.',
        });
      }
      // decoding text from bytea to UTF8
      for (let i = 0; i < articles.length; i++) {
        articles[i].body = BlogArticlesService
          .stringFromUTF8Array(articles[i].body)
          .substring(0, 350) + '...';
      }

      return res.view('blog/list', {
        title: 'Blog',
        data: articles,
        pagination: { length: pageQuantity, current: page },
      });
    });
  },


  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description show one articles*/
  async showOne(req, res) {
    let articleId = Number(req.param('id'));
    try {
      articleId = Number(req.param('id'));
    } catch (err) {
      // console.log('BlogArticles post id wrong');
    }

    await BlogArticles.findOne({
      where: { id: articleId },
    }, (err, article) => {
      if (err) {
        return res.serverError(err);
      }
      if (article === undefined) {
        return res.view('alerts/error', {
          title: 'Ошибка',
          alertMsg: 'Статья не найдена.',
          // alertMsg: 'Wrong article url.',
        });
      }
      return res.view('blog/post', { data: article });
    });
  },


};

