/**
 * @module  NomenclaturesOfGoods
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const CommonServices = require('../services/CommonServices');
// const mkdirp = require('mkdirp');
const fs = require('fs');
const path = require('path');
const fsImagesDir = 'assets/images/products/';
const dbImagesDir = '/images/products/';
module.exports = {
  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description upload images to server*/
  async uploadImages(req, res) {
    if (req.file('imgs')._files.length === 0) {
      return res.redirect(sails.config.myhost + ':8080/nomenclatures/editOne/' + id);
    } else {
      const id = req.param('id');
      const fsProductDir = fsImagesDir + id;
      const dbProductDir = dbImagesDir + id;
      const images = [];
      if (!fs.existsSync(fsProductDir)) {
        fs.mkdirSync(fsProductDir);
      }
      await req.file('imgs').upload({
        maxBytes: 100000000,
        dirname: path.resolve(sails.config.appPath, fsProductDir),
        saveAs(__newFileStream, cb) {
          const filename = __newFileStream.filename;
          images.push(dbProductDir + '/' + filename);
          cb(null, filename);
        },
      }, async(err, uploads) => {
        if (uploads.length === 0) {
          return res.badRequest('No file was uploaded');
        }
        if (err) return res.serverError(err);
        let existImages = [];
        await NomenclaturesOfGoods.findOne({
          where: { id },
        }, async(err, record) => {
          if (err) {
            return res.badRequest('err before update');
          }
          existImages = JSON.parse(CommonServices.stringFromUTF8Array(record.images));
          let mergedImages = existImages.concat(images);
          mergedImages = CommonServices.removeDuplicates(mergedImages);
          // console.log(images);
          await NomenclaturesOfGoods.update({
            where: { id },
          }).set({
            images: JSON.stringify(mergedImages),
          });
          return res.redirect(sails.config.myhost + ':8080/nomenclatures/editOne/' + id);
        });
      });
    }
  },
};
