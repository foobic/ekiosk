/**
 * @module  GoodsController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
const CommonServices = require('../services/CommonServices');
// const mkdirp = require('mkdirp');
// const fs = require('fs');
// const path = require('path');
// const fsImagesDir = 'assets/images/products/';
// const dbImagesDir = '/images/products/';
module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description show a product*/
  async showProduct(req, res) {
    // let good = await Goods.findOne({where:{id:req.param('id')}})
    await Goods.findOne({
      where: { id: req.param('id') },
    }, async(err, good) => {
      if (err) {
        return res.serverError(err);
      }
      if (good === undefined || good.quantity <= 0) {
        return res.view('alerts/error', {
          // alertMsg: 'Wrong product id',
          alertMsg: 'Неправильный id товара.',
        });
      } else {
        const goodData = await NomenclaturesOfGoods.findOne({
          id: good.nomenclatureId,
        });
        good.images = goodData.images;
        good.name = goodData.name;
        good.desc = goodData.desc;
        good.images = CommonServices.stringFromUTF8Array(good.images);
        // good.images = JSON.parse(good.images)
        return res.view('product/product', {
          title: good.name,
          good,
        });
      }
    });
  },
};
