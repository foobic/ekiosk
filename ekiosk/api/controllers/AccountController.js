/**
 * @module AccountController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing accounts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description /login Enter data for login*/
  getLoginForm(req, res) {
    return res.view('account/login', {
      title: 'Вход',
      // title: 'Login',
    });
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description if user created and verifyed -> login him*/
  async login(req, res) {
    await Users.findOne({
      email: req.param('email'),
    }).then((accountObj) => {
      // if email exists
      if (typeof accountObj !== 'undefined') {
        // if password is wrong -> return eror
        if (!accountObj.verifyPassword(req.param('password'))) {
          // return res.view('alerts/error', { alertMsg: 'Wrong password.' });
          return res.view('alerts/error', {
            alertMsg: 'Неправильный пароль.',
          });
        }
        req.session.auth = true;
        req.session.userID = accountObj.id;
        req.session.name = accountObj.name;
        req.session.email = accountObj.email;
        // if password is right -> login
        return res.redirect('/account');
      }
      // if email dosnt exist -> return error
      // return res.view('alerts/error', { alertMsg: 'Wrong email.' });
      return res.view('alerts/error', { alertMsg: 'Неправильный email.' });
    }).catch(err => res.serverError(err));
  },


  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description Todo: Send email and confirm from user.*/
  async getResetPasswordForm(req, res) {
    return res.view('account/forgotpassword', {
      title: 'Сбросить пароль',
      // title: 'Reset password',
    });
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description check input data for reset password*/
  async resetPassword(req, res) {
    const pas1 = req.param('password');
    const pas2 = req.param('password2');

    // if passwords are different -> return error
    if (pas1 !== pas2) {
      return res.view('alerts/error', {
        // alertMsg: 'Passwords are different. But should be the same',
        alertMsg: 'Введенные пароли должны быть одинаковы.',
      });
    }

    /** @param {object} req - request object
      @param {object} res - response object*/
    /** @description just find user by uniq email*/
    await Users.findOne({
      email: req.param('email'),
    }).then((accountObj) => {
      // if we got a obj and passwords are equal -> change password
      // and redirect to account view with msg
      if (typeof accountObj !== 'undefined') {
        accountObj.changePassword(pas1, () => {
          // console.log('Password Changed');
        });
        res.cookie('login', true, { maxAge: 900000, httpOnly: true });
        return res.view('account/account', {
          // title: 'Account',
          title: 'Аккаунт',
          cookies: JSON.stringify(),
          // alertMsg: 'Password successfully changed',
          alertMsg: 'Пароль успешно изменен.',
          passwordChanged: true,
          name: accountObj.name,
        });
      } else {
        // if email wrong -> return error
        return res.view('alerts/error', {
          // alertMsg: 'User with such email does not exist',
          alertMsg: 'Пользователь с таким email не существует.',
        });
      }
    }).catch(err => res.serverError(err));
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description show account page for logged user */
  async view(req, res) {
    return res.view('account/account', req.locals);
  },


  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description exit action for logged user */
  async exit(req, res) {
    req.session.destroy(err =>
      // console.log(err);
      // console.log('session destroyed');
      res.redirect('/')
    );
  },
};

