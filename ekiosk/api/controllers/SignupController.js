/**
 * @module  SignupController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing Signups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

const SignupService = require('../services/SignupService.js');

module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description  Enter data for signup*/
  getSignupForm(req, res) {
    return res.view('signup/signup', {
      // title: 'Registration page',
      title: 'Страница регистрации',
    });
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description  Unconfirmed signup*/
  async signup(req, res) {
    // redirect if user already in tempUser table
    if (await SignupService.emailExists(TempUsers, req.param('email'))) {
      return res.view('signup/userExists', {
        // title: 'Unverified. This email already exists.',
        title: 'Неверифицировано. Этот email уже существует.',
        isVerify: false,
      });
    }
    // redirect if user already registered
    if (await SignupService.emailExists(Users, req.param('email'))) {
      return res.view('signup/userExists', {
        title: 'Верифицировано. Этот email уже существует.',
        // title: 'Verified. This email already exists.',
        isVerify: true,
      });
    }

    const verifyKey = SignupService.getVerifyKey();
    await TempUsers.create({
      name: req.param('name'),
      email: req.param('email'),
      password: req.param('password'),
      verifyKey,
    }).then((Signup) => {
      const verifyLink = '/verify/' + Signup.id + '/' + verifyKey;
      return res.view('signup/send', {
        // title: 'Unconfirmed signup',
        title: 'Неподтвержденная регистрация',
        verifyLink,
        name: req.param('name'),
      });
    }).catch(err => res.serverError(err));
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description  Confirm signup*/
  async verify(req, res) {
    // console.log('===');
    const verifyUrl = req.url.split('/');

    // if user created, but not confirmed
    await TempUsers.findOne({
      id: verifyUrl[2],
      verifyKey: verifyUrl[3],
    }).then((SignupObj) => {
      // if verify key for user exits
      if (typeof SignupObj !== 'undefined') {
        const userdata = SignupObj.toJSON();
        // create new record in real user table and remove current
        // record from TempUsers table
        Users.create({
          name: userdata['name'],
          email: userdata['email'],
          password: userdata['password'],
        }).catch(err => res.serverError(err));
        SignupObj.destroy();

        return res.view('signup/confirmation', {
          // title: 'Confirmation',
          title: 'Подтверждение',
        });
      }
      // else return warning
      return res.view('alerts/warn', {
        alertMsg: 'Ключ верификации неправильный или устаревший. ' +
       ' Заметьте, ключ верификации сразу же удаляется после верификации,' +
        ' в этом случае ваш аккаунт уже активирован. ',
        // alertMsg: 'Verify key is wrong or deprecated. Note: verify key ' +
        //         'immediatly removed after first visit, in this' +
        //         'case account already activated.',
      });
    }).catch(err => res.serverError(err));
  },

};

