/**
 * @module CategoriesController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing Categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

function removeDuplicates(myArr, prop) {
  return myArr.filter((obj, pos, arr) => arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos);
}
const GOODS_ON_PAGE = 9;
module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description show category products*/
  async show(req, res) {
    const cats = await Categories.find({ select: ['id', 'name', 'parentId'] });
    const pageNum = req.param('pageNum') ? parseInt(req.param('pageNum')) : 0;
    let catName = '';
    let flag = true;
    let catId = 0;
    for (let i = 0; i < cats.length; i++) {
      if (req.param('category') === cats[i].name) {
        flag = false;
        catId = cats[i].id;
        catName = cats[i].name;
      }
    }
    if (flag) {
      return res.view('alerts/error', {
        title: 'Ошибка',
        alertMsg: 'Категория не найдена.',
      });
    } else {
      // find nomenclatures by this category
      const nomenclatures = await NomenclaturesOfGoods.find({
        where: { 'categoryId': catId },
      });

      const nomenclaturesId = nomenclatures.map(nomenclature => nomenclature.id);
      // then, find goods
      let goods = await Goods.find({
        where: { 'nomenclatureId': nomenclaturesId, 'quantity': { '>': 0 } },
      });
      const skipVal = GOODS_ON_PAGE * (pageNum - 1);
      goods = removeDuplicates(goods, 'nomenclatureId');
      const pageQuantity = Math.ceil(goods.length / GOODS_ON_PAGE);
      goods = await Goods.find({
        where: { 'nomenclatureId': nomenclaturesId, 'quantity': { '>': 0 } },
      }).skip(skipVal).limit(GOODS_ON_PAGE);
      goods = removeDuplicates(goods, 'nomenclatureId');
      if (goods.length <= 0) {
        return res.view('alerts/error', {
          title: 'Ошибка',
          alertMsg: 'Неправильная страница',
        });
      }
      for (let i = 0; i < goods.length; i++) {
        goods[i].images = JSON.parse(nomenclatures
          .filter(obj => obj.id === goods[i].nomenclatureId)[0].images
        );
        // console.log(goods[i].images );
        let myNomenclature;
        for (let j = 0, len = nomenclaturesId.length; j < len; j++) {
          if (nomenclaturesId[j] === goods[i].nomenclatureId) {
            myNomenclature = nomenclatures.map((nomenclature) => {
              if (nomenclature.id === goods[i].nomenclatureId) {
                return nomenclature;
              }
            }).filter(x => x !== undefined)[0];
            break;
          }
        }
        goods[i].name = myNomenclature.name;
      }
      return res.view('category/category', {
        title: catName,
        goods,
        pageQuantity,
        pageNum,
        catName,
      });
    }
  },
};

