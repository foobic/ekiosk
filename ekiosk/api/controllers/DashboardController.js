/**
 * @module  DashboardController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side logic for managing dashboards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description just show index page*/
  async index(req, res) {
    console.log(sails.config.myhost);
    // just get all categories from db
    const allGoods = await Goods.find({
      or: [{ 'salesFlag': 1 }, { 'popularFlag': 1 }, { 'newFlag': 1 }],
      where: { 'quantity': { '>': 0 } },
    });
    const allNomenclatures = await NomenclaturesOfGoods.find({
      select: ['id', 'name', 'images'],
    });
    allGoods.map((el) => {
      tmp = allNomenclatures.filter(nomenclature => nomenclature.id === el.nomenclatureId)[0];
      el.images = JSON.parse(tmp.images);
      el.name = tmp.name;
    });
    // console.log(allGoods);
    const salesList = allGoods.filter(el => el.salesFlag);
    const newList = allGoods.filter(el => el.newFlag);
    const popularList = allGoods.filter(el => el.popularFlag);
    // console.log(salesList);
    return res.view('index', {
      // cats: categories,
      // catsRoot: { id: 0, name: 'root', parentId: 0 },
      salesList,
      newList,
      popularList,
      title: 'BackCat | Главная',
    });
  },
};

