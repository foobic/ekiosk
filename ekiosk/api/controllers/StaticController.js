/**
 * @module  StaticController
 * @category controllers
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description  just show contacts page*/
  contacts(req, res) {
    res.view('contacts', {
      // title: "Contacts"
      title: 'Контакты',
    });
  },

  /** @param {object} req - request object
      @param {object} res - response object*/
  /** @description just show about page*/
  about(req, res) {
    res.view('about', {
      // title: "About"
      title: 'О нас',
    });
  },

};

