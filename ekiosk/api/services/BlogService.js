/**
 * @module Blog
 * @category services
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
// Thanks
// https://weblog.rogueamoeba.com/2017/02/27/javascript-correctly-converting-a-byte-array-to-a-utf-8-string/


const BlogService = {
  stringFromUTF8Array(data) {
    const extraByteMap = [1, 1, 1, 1, 2, 2, 3, 0];
    const count = data.length;
    let str = '';
    for (let index = 0; index < count;) {
      let ch = data[index++];
      if (ch & 0x80) {
        let extra = extraByteMap[(ch >> 3) & 0x07];
        if (!(ch & 0x40) || !extra || ((index + extra) > count)) {
          return null;
        }
        ch &= (0x3F >> extra);
        for (;extra > 0; extra -= 1) {
          const chx = data[index++];
          if ((chx & 0xC0) !== 0x80) {
            return null;
          }
          ch = (ch << 6) | (chx & 0x3F);
        }
      }
      str += String.fromCharCode(ch);
    }
    return str;
  },
};


module.exports = BlogService;
