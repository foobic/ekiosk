/**
 * @module notImplementedError
 * @category services
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
/* Goods*
 *  * Sets no-cache header in response.
 *   */
const NotImplementedError = function(message, data = []) {
  this.name = 'NotImplementedError';
  this.message = (message || '');
  this.data = data;
};

NotImplementedError.prototype = Error.prototype;

module.exports = NotImplementedError;
