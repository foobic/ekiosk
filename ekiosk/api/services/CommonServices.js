/**
 * @module Common
 * @category services
 * @license MIT
 * @author Alex Gorbov <foobic@gmail.com>
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
const CommonServices = {
  // JSON.parse(bfilters.toString()) also work but worst
  stringFromUTF8Array(data) {
    const extraByteMap = [1, 1, 1, 1, 2, 2, 3, 0];
    const count = data.length;
    let str = '';
    for (let index = 0; index < count;) {
      let ch = data[index++];
      if (ch & 0x80) {
        let extra = extraByteMap[(ch >> 3) & 0x07];
        if (!(ch & 0x40) || !extra || ((index + extra) > count)) {
          return null;
        }
        ch &= (0x3F >> extra);
        for (;extra > 0; extra -= 1) {
          const chx = data[index++];
          if ((chx & 0xC0) !== 0x80) {
            return null;
          }
          ch = (ch << 6) | (chx & 0x3F);
        }
      }
      str += String.fromCharCode(ch);
    }
    return str;
  },
  removeDuplicates(arr) {
    const seen = {};
    const retArr = [];
    for (let i = 0; i < arr.length; i++) {
      if (!(arr[i] in seen)) {
        retArr.push(arr[i]);
        seen[arr[i]] = true;
      }
    }
    return retArr;
  },
};
module.exports = CommonServices;
