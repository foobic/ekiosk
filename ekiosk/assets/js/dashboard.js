$(document).ready(() => {
  if (window.localStorage.getItem('orderDone') === '1') {
    $('.alertWrapper').prepend(' <div class="container p-5"> <div class="alert alert-success text-center p-5" role="alert"> <strong>Ваш заказ принят. Ожидайте звонка в ближайшее время.</strong> </div> </div> ');
    window.localStorage.setItem('orderDone', '0');
  }
  function categories() {
    const catsTree = addRoot(listToTree(cats), catsRoot);

    let level = 0;
    const optionValues = [];
    function my_recoursion(arr) {
      Object.keys(arr).forEach((key, index) => {
        optionValues.push([
          arr[key].id,
          ' >>> '.repeat(level) + arr[key].name,
          arr[key].name,
        ]);
        if (arr[key]['children'] !== []) {
          level += 1;
          my_recoursion(arr[key]['children'], level);
          level -= 1;
        }
      });
    }
    my_recoursion(catsTree);
    const myList = $('.categories-list')[0];
    for (let i = 1; i < optionValues.length; i++) {
    // if(optionValues[i][0] === category.parentId ) {
      $(myList).append($('<a></option>')
        .attr({
          'value': optionValues[i][0],
          'class': 'text-primary d-block categoriesLinks',
          'href': '/category/' + optionValues[i][2],
        })
        .text(optionValues[i][1]));
    }
  }
});
