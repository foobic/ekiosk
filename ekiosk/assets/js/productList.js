$(window).on('load', (fd) => {
  $('.productAddToCart').on('click', function(e) {
    e.preventDefault();
    const cart = $('.middleheader-basket');
    const productItem = $(this).parents('.product-item');
    const imgtodrag = $(productItem).find('img').eq(0);
    const productId = $(productItem).data('product-id');
    const productPrice = $(productItem).data('product-price');
    const productName = $(productItem).data('product-name');
    // let productQty = $(productItem).data('product-qty');
    const productQty = 1;


    jQuery.ajax({
      url: '/addToBasket',
      type: 'POST',
      data: {
        'id': productId,
        'price': productPrice,
        'name': productName,
        'qty': productQty,
      },
      success(response, textStatus, jqXHR) {
        const sum = parseFloat(parseFloat($($(cart).find('.basketSum')[0]).text()) + parseFloat(parseFloat(productPrice))).toFixed(2);
        $($(cart).find('.basketSum')[0]).text(sum);
        const qty = parseFloat($($(cart).find('.basketQty')[0]).text()) + parseFloat(productQty);
        $($(cart).find('.basketQty')[0]).text(qty);
        // window.alert("Yay!");
      },
      error(jqXHR, textStatus, errorThrown) {
        console.log('adding ERRRRRRRRR');
        window.alert(textStatus, errorThrown);
      },
    });
    const offsetX = Number(($($(imgtodrag)[0]).width() / 2).toFixed(0));
    const offsetY = Number(($($(imgtodrag)[0]).height() / 2).toFixed(0));

    if (imgtodrag) {
      const imgclone = imgtodrag.clone()
        .offset({
          left: imgtodrag.offset().left + offsetX / 2,
          top: imgtodrag.offset().top + offsetY*2
          // bottom: imgtodrag.offset().top,
        })
        .css({
          'opacity': '0.5',
          'position': 'absolute',
          'height': '150px',
          'width': '150px',
          'z-index': '100',
        })
        .appendTo($('body'))
        .animate({
          'top': cart.offset().top + 10,
          'left': cart.offset().left + 10,
          'width': 75,
          'height': 75,
        }, 500, 'easeInOutExpo');
      setTimeout(() => {
        cart.effect('shake', {
          times: 1,
        }, 100);
      }, 200);
      imgclone.animate({
        'width': 0,
        'height': 0,
      }, function() {
        $(this).detach();
      });
    }
  });
});
