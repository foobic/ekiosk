$(window).on('ready', () => {

  // $('body').LoadingOverlay("show")
});

$(window).on('load', () => {
  // $('body').LoadingOverlay("show")

  // console.log(session);
  if (session) {
    $($('.addedToCart')[0]).show();
    $($('.addedToCart span')).show();
  } else {
    $($('.addToCartBlock')[0]).show();
    $($('.addToCartBlock .btn')).show();
  }
  if (typeof good !== 'undefined') {
    const images = JSON.parse(good.images);
    // console.log(images)
    let img,
        a;
    for (let i = 0; i < images.length; i++) {
      path = images[i];
      // console.log(path);
      a = $('<a />').attr('href', path);
      img = $('<img />').attr('src', path);
      $(a).append(img);
      if (i === 0) {
        $(a).attr('class', 'current');
        $($('.photo')).append($(img).clone());
      }
      $($('.mySlider_thumb')[0]).append($(a));
    }
    $('.mySlider_thumb').on('click', 'a', function() {
      $(this).attr('id', 'current').siblings().removeAttr('id');
      $('.photo img').attr('src', $(this).prop('href'));
      return false;
    });
  }
  $('.addToCart').on('click', function() {
    const productId = parseInt($(this).data('product-id'));
    const productPrice = $(this).data('product-price');
    const productName = $(this).data('product-name');
    $($(this).closest('.addToCartBlock')[0]).remove();
    $($('.addedToCart')[0]).show();
    $($('.addedToCart span')).show();
    jQuery.ajax({
      url: '/addToBasket',
      type: 'POST',
      data: {
        'id': productId,
        'price': productPrice,
        'name': productName,
        'qty': 1,
      },
      success(response, textStatus, jqXHR) {
        const cart = $('.basket_text')[0];
        const sum = parseFloat(parseFloat($($(cart).find('.basketSum')[0]).text()) + parseFloat(parseFloat(productPrice))).toFixed(2);
        $($(cart).find('.basketSum')[0]).text(sum);
        const qty = parseInt($($(cart).find('.basketQty')[0]).text()) + 1;
        console.log(parseInt($($(cart).find('.basketQty')[0]).text()));
        $($(cart).find('.basketQty')[0]).text(qty);
        // window.alert("Yay!");
        console.log('fasdkd;fsa');
      },
      error(jqXHR, textStatus, errorThrown) {
        console.log('adding ERRRRRRRRR');
        window.alert(textStatus, errorThrown, jqXHR);
      },
    });
  });
  // $('body').LoadingOverlay("hide")
  // $('.slider_priceBlock').slideDown(100)
  // $('.mySlider').slideDown(100)
  // $('.priceBlock').slideDown(100)
});

