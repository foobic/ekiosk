/* global userId */
$(window).on('load', () => {
  // console.log(basket)
  // console.log(basketData)
  $('input[name=username]').val(window.localStorage.getItem('username'));
  $('input[name=userphone]').val(window.localStorage.getItem('userphone'));
  function refreshCartView(basketData) {
    console.log('cart view: ', basketData);
    $($('span.basketQty')[0]).text(basketData.qty);
    $($('span.basketSum')[0]).text(basketData.sum);
  }
  function refreshBasketSession(basket, basketData) {
    console.log('>>>', basket);
    console.log('>>>', basketData);
    jQuery.ajax({
      url: '/basket/refreshBasket',
      type: 'POST',
      data: { basket, basketData },
      success(response, textStatus, jqXHR) {
        // window.alert('Basket Refreshed');
      },
      error(jqXHR, textStatus, errorThrown) {
        window.alert(jqXHR.responseJSON.error);
      },
    });
  }

  /* Assign actions */
  $($('.refreshBasketBtn')[0]).click((e) => {
    // window.alert('lol');
    $($('.refreshBasketBtn')[0]).prop('disabled', true);
    $($('.checkoutBtn')[0]).prop('disabled', false);

    const srcItems = $('.productInCart');
    const basket = [];
    const basketData = {};
    let totalSum = 0;
    let totalQty = 0;
    let obj = null;
    $.each(srcItems, (index, el) => {
      obj = {};
      let tmpSum = 0;
      obj.id = parseInt($(el).attr('data-productId'));
      obj.price = parseFloat($($(el).find('span[data-price=1]')[0]).text());
      // console.log($($(el).find('span[data-price=1]')[0]).text());
      obj.name = $($(el).find('span[data-name=1]')[0]).text();
      obj.qty = parseInt($($(el).find('input[data-qty=1]')[0]).val());
      tmpSum = parseFloat(obj.qty * obj.price);
      totalSum += tmpSum;
      totalQty += obj.qty;
      basket.push(obj);
      $($(el).find('span[data-total=1]')[0]).text(parseFloat(tmpSum).toFixed(2));
      // console.log(obj);
    });
    console.log(basket);
    basketData.qty = totalQty;
    basketData.sum = totalSum;
    // console.log(totalSum);

    $($('td[data-totalSumCart=1] span')[0]).text(parseFloat(totalSum).toFixed(2));
    $($('td[data-totalQtyCart=1] span')[0]).text(totalQty);
    refreshBasketSession(basket, basketData);
    refreshCartView(basketData);
  });
  $('.productInCart input[name=qty]').change((e) => {
    console.log($('.checkoutBtn')[0]);
    $($('.refreshBasketBtn')[0]).prop('disabled', false);
    $($('.checkoutBtn')[0]).prop('disabled', true);
  });


  // $('.deleteProduct').click(function() {
  //   const deleteId = parseInt($(this).attr('data-productId'));
  //   const thisProduct = this;
  //   jQuery.ajax({
  //     url: '/basket/deleteOne',
  //     type: 'POST',
  //     data: {
  //       'id': deleteId,
  //     },
  //     success(response, textStatus, jqXHR) {
  //       // window.location.href = '/';
  //       if ($($($(thisProduct).parents('tbody')).find('tr')).length === 1) {
  //         window.location.href = '/';
  //       } else {
  //         // window.location.href = '/basket';
  //         console.log(response, textStatus, jqXHR);
  //         const itemQty = parseInt($($(thisProduct)
  //           .parents('tr')[0]).find('input[data-qty=1]').val());
  //         const itemPrice = parseInt($($(thisProduct)
  //           .parents('tr')[0]).find('span[data-price=1]').text());
  //         const allQty = parseInt($('.basketQty').text());
  //         $('.basketQty').text(allQty - itemQty);
  //         $($(thisProduct).parents('tr')[0]).hide('slow', () => {
  //           $($(thisProduct).parents('tr')[0]).remove();
  //         });
  //       }
  //     },
  //     error(jqXHR, textStatus, errorThrown) {
  //       window.location.href = '/basket';
  //       // window.alert(jqXHR);
  //       //   console.log(jqXHR);
  //     },
  //   });
  // });
  $('.deleteProduct').click(function() {
    const deleteId = parseInt($(this).attr('data-productId'));
    const thisProduct = this;
    $($('.refreshBasketBtn')[0]).prop('disabled', false);
    $($('.checkoutBtn')[0]).prop('disabled', true);
    console.log($($($(thisProduct).parents('tbody')).find('tr')).length);
    if ($($($(thisProduct).parents('tbody')).find('tr')).length === 2) {
      //       !!!!!!!!!!! REFRESH BASKET
      refreshBasketSession([], { qty: 0, sum: 0 });
      refreshCartView({ qty: 0, sum: 0 });
      window.location.href = '/';
    } else {
      const itemQty = parseInt($($(thisProduct)
        .parents('tr')[0]).find('input[data-qty=1]').val());
      const itemPrice = parseInt($($(thisProduct)
        .parents('tr')[0]).find('span[data-price=1]').text());
      const allQty = parseInt($('.basketQty').text());
      $('.basketQty').text(allQty - itemQty);
      $($(thisProduct).parents('tr')[0]).hide('slow', () => {
        $($(thisProduct).parents('tr')[0]).remove();
      });
    }
  });

  $('.checkoutBtn').click(() => {
    const userphone = $('input[name=userphone]').val();
    const username = $('input[name=username]').val();
    const useremail = $('input[name=useremail]').val();
    if (username.name < 2 || userphone.length != 19) {
      window.alert('Неправильно введены имя и/или телефон');
      return;
    }
    const userData = {
      name: username,
      phone: userphone,
      email: useremail,
    };
    const allProducts = $('.productInCart');
    const order = { 'customer': 0, 'items': [], 'sum': 0 };
    let totalSum = 0;
    let item;
    for (let i = 0; i < allProducts.length; i++) {
      item = {};
      item.id = parseInt($(allProducts[i]).data('productid'));
      item.quantity = parseInt($(allProducts[i])
        .find('input[data-qty=1]').val());
      item.price = parseFloat($(allProducts[i])
        .find('span[data-price=1]').text());
      totalSum = (parseFloat(totalSum) +
          parseFloat(item.price * item.quantity)).toFixed(2);
      order.items.push(item);
    }
    order.sum = parseFloat(totalSum);
    order.customer = userId;
    // console.log(order);
    jQuery.ajax({
      url: '/checkout',
      type: 'POST',
      data: { order, userData },
      success(response, textStatus, jqXHR) {
        window.localStorage.setItem('username', username);
        window.localStorage.setItem('userphone', userphone);
        window.localStorage.setItem('orderDone', '1');
        // window.alert('Yay!');
        // window.location.reload();
        window.location.replace('/');
        // console.log(response, textStatus, jqXHR);
      },
      error(jqXHR, textStatus, errorThrown) {
        // console.log('adding ERRRRRRRRR');
        const data = jqXHR.responseJSON.data;
        // console.log(data);
        for (let i = 0; i < data.length; i++) {
          $($($('tr[data-productid=' + data[i].id + ']')[0]).find('input[data-qty=1]')[0]).val(data[i].qty);
        }
        //         data.map((el)=>{
        //           console.log(el.qty);
        //           console.log($($($('tr[data-productid='+el.id+']')[0]).find('input[data-qty=1]')[0]));
        // $($($('tr[data-productid='+el.id+']')[0]).find('input[data-qty=1]')[0]).val(el.qty)
        //           // console.log()
        //           return el;
        //         })
        $($('.refreshBasketBtn')[0]).prop('disabled', false);
        $($('.checkoutBtn')[0]).prop('disabled', true);
        window.alert(jqXHR.responseJSON.error);
      },
    });
  });
});

