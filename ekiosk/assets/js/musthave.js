$(window).on('load', (fd) => {
// $("footer").css({"padding-top":Math.abs($(window).height()-$("body").height())})

});
if ($(window).height() - $('body').height() > 0) {
  $('footer').css({ 'padding-top': $(window).height() - $('body').height() });
}

function listToTree(list) {
  let map = {},
      node,
      roots = [],
      i;
  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i; // initialize the map
    list[i].children = []; // initialize the children
  }
  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.parentId !== 0) {
      if (typeof list[map[node.parentId]] === 'undefined') {
        window.alert('Uncorrect parentId: ' + node.id);
        break;
      }
      list[map[node.parentId]].children.push(node);
    } else {
      roots.push(node);
    }
    // console.log(node);
  }
  return roots;
}

function addRoot(tree, root) {
  const newList = [root];
  newList[0].children = tree;
  return newList;
}
$('.middleheader-basket').on('click', () => {
  window.location = '/basket';
  return false;
});
