/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
   * etc. depending on your default view engine) your home page.              *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  '/': {
    controller: 'Dashboard',
    action: 'index'
  },

  /***************************************************************************
   *                                                                          *
   * Custom routes here...                                                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the custom routes above, it   *
   * is matched against Sails route blueprints. See `config/blueprints.js`    *
   * for configuration options and examples.                                  *
   *                                                                          *
   ***************************************************************************/
  // SIGNUP
  // 'GET /signup': {
  //   controller: 'Signup',
  //   action: 'getSignupForm'
  // },
  // 'POST /signup': {
  //   controller: 'Signup',
  //   action: 'signup'
  // },
  // 'GET /verify#<{(|': {
  //   controller: 'Signup',
  //   action: 'verify'
  // },
  // SIGNUP END



  //Account
  // 'GET /login': {
  //   controller: 'Account',
  //   action: 'getLoginForm'
  // },
  // 'POST /login': {
  //   controller: 'Account',
  //   action: 'login'
  // },
  // 'GET /forgotpassword': {
  //   controller: 'Account',
  //   action: 'getResetPasswordForm'
  // },
  // 'POST /forgotpassword': {
  //   controller: 'Account',
  //   action: 'resetPassword'
  // },
  // 'GET /account': {
  //   controller: 'Account',
  //   action: 'view',
  //   locals: {
  //     title: 'Account'
  //   }
  // },
  // 'GET /account/exit': {
  //   controller: 'Account',
  //   action: 'exit',
  // },
  //Account END


  // BLOG
  // 'GET /blog': { controller: 'BlogArticles', action: 'showAll', },
  // 'GET /blog/:page': {
  //   controller: 'BlogArticles',
  //   action: 'showAll',
  // },
  // 'GET /article/:id': {
  //   controller: 'BlogArticles',
  //   action: 'showOne',
  // },
  // BLOG END

  'GET /contacts': {
    controller: 'Static',
    action: 'contacts',
  },
  // locals:{
  //     title:'Contacts'
  // }
  'GET /about': {
    controller: 'Static',
    action: 'about',
  },
  // locals:{
  //     title:'About us'
  // }



  // 'GET /product': {
  //   view: 'product/product'
  // },
  // 'GET /category': {
  //   view: 'category/category'
  // },

  'GET /product/:id': {
    controller: 'Goods',
    action: 'showProduct',
  },
  'GET /category/:category/:pageNum': {
    controller: 'Categories',
    action: 'show',
  },
  'GET /category/:category': {
    controller: 'Categories',
    action: 'show',
  },

  'POST /addToBasket': {
    controller: 'Basket',
    action: 'add',
  },
  'GET /basket': {
    controller: 'Basket',
    action: 'show',
  },
  'POST /basket/deleteOne': {
    controller: 'Basket',
    action: 'deleteOne',
  },
  'POST /checkout': {
    controller: 'Basket',
    action: 'checkout',
  },
  'POST /uploadImages/:id': {
    controller: 'NomenclaturesOfGoods',
    action: 'uploadImages',
  },
  'POST /basket/refreshBasket': {
    controller: 'Basket',
    action: 'refreshBasket',
  },


};
