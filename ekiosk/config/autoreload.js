module.exports.autoreload = {
    active: false,
    usePolling: false,
    overrideMigrateSetting: false,
    dirs: [
        "api/models",
        "api/controllers",
        "api/services",
        "config/locales",
        "views",
        "assets"
    ],
    ignored: [
        // Ignore all files with .ts extension
        "**.ts"
    ]
}
