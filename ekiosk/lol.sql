--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: BlogArticles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."BlogArticles" (
    id integer NOT NULL,
    title text,
    body bytea,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."BlogArticles" OWNER TO postgres;

--
-- Name: BlogArticles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."BlogArticles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."BlogArticles_id_seq" OWNER TO postgres;

--
-- Name: BlogArticles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."BlogArticles_id_seq" OWNED BY public."BlogArticles".id;


--
-- Name: Categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Categories" (
    id integer NOT NULL,
    name text,
    "parentId" integer,
    filters bytea,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Categories" OWNER TO postgres;

--
-- Name: Categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Categories_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Categories_id_seq" OWNER TO postgres;

--
-- Name: Categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Categories_id_seq" OWNED BY public."Categories".id;


--
-- Name: Delivers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Delivers" (
    id integer NOT NULL,
    name text,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Delivers" OWNER TO postgres;

--
-- Name: Delivers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Delivers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Delivers_id_seq" OWNER TO postgres;

--
-- Name: Delivers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Delivers_id_seq" OWNED BY public."Delivers".id;


--
-- Name: Goods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Goods" (
    id integer NOT NULL,
    quantity integer,
    discount integer,
    price real,
    "realPrice" real,
    "salesFlag" integer,
    "newFlag" integer,
    "popularFlag" integer,
    "nomenclatureId" integer,
    "providerId" integer,
    "deliverId" integer,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Goods" OWNER TO postgres;

--
-- Name: Goods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Goods_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Goods_id_seq" OWNER TO postgres;

--
-- Name: Goods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Goods_id_seq" OWNED BY public."Goods".id;


--
-- Name: NomenclaturesOfGoods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."NomenclaturesOfGoods" (
    id integer NOT NULL,
    name text,
    "desc" text,
    manufactor text,
    "categoryId" integer,
    properties bytea,
    "customProperties" bytea,
    images bytea,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."NomenclaturesOfGoods" OWNER TO postgres;

--
-- Name: NomenclaturesOfGoods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."NomenclaturesOfGoods_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."NomenclaturesOfGoods_id_seq" OWNER TO postgres;

--
-- Name: NomenclaturesOfGoods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."NomenclaturesOfGoods_id_seq" OWNED BY public."NomenclaturesOfGoods".id;


--
-- Name: Orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Orders" (
    id integer NOT NULL,
    customer integer,
    "orderDesc" text,
    items bytea,
    sum real,
    "statusCode" integer,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Orders" OWNER TO postgres;

--
-- Name: Orders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Orders_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Orders_id_seq" OWNER TO postgres;

--
-- Name: Orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Orders_id_seq" OWNED BY public."Orders".id;


--
-- Name: Providers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Providers" (
    id integer NOT NULL,
    name text,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Providers" OWNER TO postgres;

--
-- Name: Providers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Providers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Providers_id_seq" OWNER TO postgres;

--
-- Name: Providers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Providers_id_seq" OWNED BY public."Providers".id;


--
-- Name: TempUsers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."TempUsers" (
    id integer NOT NULL,
    name text,
    email text,
    "verifyKey" text,
    password text,
    "lastEnter" date,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."TempUsers" OWNER TO postgres;

--
-- Name: TempUsers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."TempUsers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."TempUsers_id_seq" OWNER TO postgres;

--
-- Name: TempUsers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."TempUsers_id_seq" OWNED BY public."TempUsers".id;


--
-- Name: Users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Users" (
    id integer NOT NULL,
    "userDesc" text,
    name text,
    phone text,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone
);


ALTER TABLE public."Users" OWNER TO postgres;

--
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_id_seq" OWNER TO postgres;

--
-- Name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;


--
-- Name: BlogArticles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."BlogArticles" ALTER COLUMN id SET DEFAULT nextval('public."BlogArticles_id_seq"'::regclass);


--
-- Name: Categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categories" ALTER COLUMN id SET DEFAULT nextval('public."Categories_id_seq"'::regclass);


--
-- Name: Delivers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Delivers" ALTER COLUMN id SET DEFAULT nextval('public."Delivers_id_seq"'::regclass);


--
-- Name: Goods id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Goods" ALTER COLUMN id SET DEFAULT nextval('public."Goods_id_seq"'::regclass);


--
-- Name: NomenclaturesOfGoods id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."NomenclaturesOfGoods" ALTER COLUMN id SET DEFAULT nextval('public."NomenclaturesOfGoods_id_seq"'::regclass);


--
-- Name: Orders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Orders" ALTER COLUMN id SET DEFAULT nextval('public."Orders_id_seq"'::regclass);


--
-- Name: Providers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Providers" ALTER COLUMN id SET DEFAULT nextval('public."Providers_id_seq"'::regclass);


--
-- Name: TempUsers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TempUsers" ALTER COLUMN id SET DEFAULT nextval('public."TempUsers_id_seq"'::regclass);


--
-- Name: Users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);


--
-- Data for Name: BlogArticles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."BlogArticles" (id, title, body, "createdAt", "updatedAt") FROM stdin;
1	Здравствуйте	\\x30392e30362e31380d0ad09cd18b20d0bed182d0bad180d18bd0bbd0b8d181d18c2121210d0ad0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d0902120d0a3d0a0d09021200d0a0d0a	2018-06-09 19:27:13+03	2018-06-09 19:27:13+03
2	выфаыва	\\x20202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020202020	2018-06-09 19:27:35+03	2018-06-09 19:27:35+03
\.


--
-- Name: BlogArticles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."BlogArticles_id_seq"', 2, true);


--
-- Data for Name: Categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Categories" (id, name, "parentId", filters, "createdAt", "updatedAt") FROM stdin;
2	backpacks	0	\\x7b7d	2018-06-30 19:27:10+03	2018-06-30 19:27:10+03
3	bags	0	\\x7b7d	2018-06-30 20:05:10+03	2018-06-30 20:05:10+03
4	accessories	0	\\x7b7d	2018-06-30 20:05:21+03	2018-06-30 20:05:21+03
\.


--
-- Name: Categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Categories_id_seq"', 4, true);


--
-- Data for Name: Delivers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Delivers" (id, name, "createdAt", "updatedAt") FROM stdin;
2	Поставка №1	2018-06-09 19:12:31+03	2018-06-30 19:40:15+03
3	Поставка №2	2018-06-30 20:07:47+03	2018-06-30 20:07:47+03
4	Поставка №3	2018-06-30 20:14:56+03	2018-06-30 20:14:56+03
\.


--
-- Name: Delivers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Delivers_id_seq"', 4, true);


--
-- Data for Name: Goods; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Goods" (id, quantity, discount, price, "realPrice", "salesFlag", "newFlag", "popularFlag", "nomenclatureId", "providerId", "deliverId", "createdAt", "updatedAt") FROM stdin;
21	15	10	100	50	0	0	1	15	2	4	2018-06-30 20:15:17+03	2018-06-30 20:17:46+03
22	4	15	200	150	0	1	0	14	3	4	2018-06-30 20:15:47+03	2018-06-30 20:19:59+03
23	67	5	59	25	1	0	0	13	3	4	2018-06-30 20:16:06+03	2018-07-01 11:00:12+03
20	45	10	600	250	1	0	0	10	3	3	2018-06-30 20:09:07+03	2018-07-01 11:07:24+03
18	97	5	700	500	0	0	1	11	3	3	2018-06-30 20:08:15+03	2018-07-01 12:01:04+03
19	13	0	750	400	0	0	1	12	3	3	2018-06-30 20:08:43+03	2018-07-01 13:25:18+03
17	100	10	290	150	1	0	0	9	2	2	2018-06-30 20:00:53+03	2018-07-01 13:26:29+03
15	100	15	200	100	0	1	0	7	2	2	2018-06-30 19:40:35+03	2018-07-01 13:26:33+03
16	100	0	450	250	0	1	0	8	2	2	2018-06-30 19:53:55+03	2018-07-01 13:26:38+03
\.


--
-- Name: Goods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Goods_id_seq"', 23, true);


--
-- Data for Name: NomenclaturesOfGoods; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."NomenclaturesOfGoods" (id, name, "desc", manufactor, "categoryId", properties, "customProperties", images, "createdAt", "updatedAt") FROM stdin;
7	Черный рюкзак	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	2	\N	\N	\\x5b222f696d616765732f70726f64756374732f372f312e6a7067225d	2018-06-30 19:28:12+03	2018-06-30 19:40:53+03
11	Коричневая женская сумка	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	3	\N	\N	\\x5b222f696d616765732f70726f64756374732f31312f352e6a7067225d	2018-06-30 20:06:20+03	2018-06-30 20:06:32+03
8	Рюкзак Jansport	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	2	\N	\N	\\x5b222f696d616765732f70726f64756374732f382f322e706e67225d	2018-06-30 19:52:39+03	2018-06-30 19:58:53+03
9	Рюкзак Динамо Киев	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n	Ukraine	2	\N	\N	\\x5b222f696d616765732f70726f64756374732f392f332e6a7067225d	2018-06-30 20:00:24+03	2018-06-30 20:02:12+03
12	Сумка-рюкзак	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	3	\N	\N	\\x5b222f696d616765732f70726f64756374732f31322f362e6a7067225d	2018-06-30 20:07:04+03	2018-06-30 20:09:26+03
10	Зеленая женская сумка	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	3	\N	\N	\\x5b222f696d616765732f70726f64756374732f31302f342e6a7067225d	2018-06-30 20:04:02+03	2018-06-30 20:05:48+03
15	Браслет	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n	Ukraine	4	\N	\N	\\x5b222f696d616765732f70726f64756374732f31352f392e6a7067225d	2018-06-30 20:13:33+03	2018-06-30 20:13:52+03
14	Очки	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n	Ukraine	4	\N	\N	\\x5b222f696d616765732f70726f64756374732f31342f382e6a706567225d	2018-06-30 20:13:22+03	2018-06-30 20:14:04+03
13	Брелок	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.	Ukraine	4	\N	\N	\\x5b222f696d616765732f70726f64756374732f31332f372e6a7067225d	2018-06-30 20:11:44+03	2018-06-30 20:14:16+03
\.


--
-- Name: NomenclaturesOfGoods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."NomenclaturesOfGoods_id_seq"', 15, true);


--
-- Data for Name: Orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Orders" (id, customer, "orderDesc", items, sum, "statusCode", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: Orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Orders_id_seq"', 1, true);


--
-- Data for Name: Providers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Providers" (id, name, "createdAt", "updatedAt") FROM stdin;
2	Поставщик Петя	2018-06-09 19:20:01+03	2018-06-30 19:40:06+03
3	Поставщик Вася	2018-06-30 20:07:28+03	2018-06-30 20:07:28+03
\.


--
-- Name: Providers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Providers_id_seq"', 3, true);


--
-- Data for Name: TempUsers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."TempUsers" (id, name, email, "verifyKey", password, "lastEnter", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: TempUsers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."TempUsers_id_seq"', 1, false);


--
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Users" (id, "userDesc", name, phone, "createdAt", "updatedAt") FROM stdin;
\.


--
-- Name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Users_id_seq"', 17, true);


--
-- Name: BlogArticles BlogArticles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."BlogArticles"
    ADD CONSTRAINT "BlogArticles_pkey" PRIMARY KEY (id);


--
-- Name: Categories Categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Categories"
    ADD CONSTRAINT "Categories_pkey" PRIMARY KEY (id);


--
-- Name: Delivers Delivers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Delivers"
    ADD CONSTRAINT "Delivers_pkey" PRIMARY KEY (id);


--
-- Name: Goods Goods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Goods"
    ADD CONSTRAINT "Goods_pkey" PRIMARY KEY (id);


--
-- Name: NomenclaturesOfGoods NomenclaturesOfGoods_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."NomenclaturesOfGoods"
    ADD CONSTRAINT "NomenclaturesOfGoods_pkey" PRIMARY KEY (id);


--
-- Name: Orders Orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Orders"
    ADD CONSTRAINT "Orders_pkey" PRIMARY KEY (id);


--
-- Name: Providers Providers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Providers"
    ADD CONSTRAINT "Providers_pkey" PRIMARY KEY (id);


--
-- Name: TempUsers TempUsers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."TempUsers"
    ADD CONSTRAINT "TempUsers_pkey" PRIMARY KEY (id);


--
-- Name: Users Users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

