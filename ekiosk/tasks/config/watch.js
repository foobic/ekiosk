// tasks/config/watch.js
module.exports = function(grunt) {

    grunt.config.set('watch', {
        api: {

            // API files to watch:
            files: ['api/**/*']
        },
        assets: {

            // Assets to watch:
            files: ['assets/**/*','tasks/pipeline.js', 'views/**/*'],
            //files: ['assets/js/**/*','assets/style/**/*' ,'tasks/pipeline.js', 'views/**/*'],
            // options: {
            //     livereload: false
            // },

            // When assets are changed:
            tasks: ['syncAssets' , 'linkAssets'],
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
};
